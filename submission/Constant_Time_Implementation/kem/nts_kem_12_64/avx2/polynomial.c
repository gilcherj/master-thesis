/**
 *  polynomial.c
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(12, 64)
 *  Platform:  AVX2
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "polynomial.h"
#include "bits.h"
#include "random.h"
#include "mem.h"
#ifdef BENCHMARK
    #include "timing.h"
#endif

#define COPY_POLY(a, b)  (b)->degree = (a)->degree;\
                         memcpy((b)->coeff, (a)->coeff, (a)->size*sizeof(ff_unit))

poly* init_poly(int size)
{
    poly* px = (poly *)malloc(sizeof(poly));
    if (!px)
        return NULL;
    
    px->size = size;
    px->degree = -1; /* Indicate a zero polynomial */
    px->coeff = (ff_unit *)calloc(size, sizeof(ff_unit));
    if (!px->coeff)
        return NULL;
    
    return px;
}

void free_poly(poly* px)
{
    if (px) {
        if (px->coeff)
            free(px->coeff);
        px->coeff = NULL;
        px->degree = -1;
        px->size = 0;
        free(px);
    }
}

void zero_poly(poly* px)
{
    CT_memset(px->coeff, 0, px->size * sizeof(ff_unit));
}

poly* clone_poly(const poly *px)
{
    poly *qx = init_poly(px->size);
    if (!qx)
        return NULL;
    
    memcpy(qx->coeff, px->coeff, px->size*sizeof(ff_unit));
    qx->degree = px->degree;
    
    return qx;
}

int is_equal_poly(const poly* ax, const poly* bx)
{
    if (ax->degree != bx->degree)
        return 0;
    return (0 == memcmp(ax->coeff, bx->coeff, (ax->degree+1)*sizeof(ff_unit)));
}

void update_poly_degree(poly *px)
{
    px->degree = px->size-1;
    while (px->degree > 0 && !px->coeff[px->degree]) --px->degree;
}

poly* create_random_poly(const FF2m* ff2m, int degree)
{
    int i;
    ff_unit mask = (1 << ff2m->m)-1;
    poly* px = init_poly(degree+1);
    if (!px)
        return NULL;
    
    px->degree = degree;
    randombytes((uint8_t *)px->coeff, px->degree*sizeof(ff_unit));
    px->coeff[px->degree] = 1;
    
    /* Make sure that the leading coefficient is non zero */
    while (!px->coeff[0])
        randombytes((uint8_t *)px->coeff, sizeof(ff_unit));
    
    /**
     * Mask the coefficient so that it does not go over 
     * the highest finite field value 
     **/
    i = px->size; do { --i;
        px->coeff[i] &= mask;
    } while (i);
    
    return px;
}

ff_unit evaluate_poly(const FF2m* ff2m, const poly* px, ff_unit a)
{
    int i;
    ff_unit x, y;
    if (!a) { /* a is zero */
        return px->coeff[0];
    }
    /* ... a is a non zero */
    x = (ff_unit)a;
    y = px->coeff[0];
    for (i=1; i<=px->degree; i++)
    {
        y = ff2m->ff_add(ff2m, y,
                         ff2m->ff_mul(ff2m, px->coeff[i], x));
        x = ff2m->ff_mul(ff2m, x, a);
    }
    return y;
}

int formal_derivative_poly(const poly* fx, poly *dx)
{
#ifdef BENCHMARK
    tic(&keygenFormalDerivative);
#endif
    int i;
    /* Make sure that d(x) has enough buffer space */
    if (dx->size < fx->size-1)
        return 0;
    
    for (i=0; i<fx->degree; ++i)
    {
        dx->coeff[i] = 0;
        if ((i & 1) == 0)
            dx->coeff[i] = fx->coeff[i+1];
    }
    dx->degree = fx->degree-1;

    int zero = 1;
    for (i = 0; i < fx->degree; i++) {
        zero &= CT_is_equal_zero(dx->coeff[fx->degree-1-i]);
        dx->degree -= zero;
    }
#ifdef BENCHMARK
    toc(&keygenFormalDerivative);
#endif
    return 1;
}

void modulo_reduce_poly(const FF2m* ff2m, const poly *mx, poly *ax)
{
    int i, j;
    ff_unit a;
    
    while (ax->degree >= mx->degree) {
        a = ff2m->ff_mul(ff2m, ax->coeff[ax->degree],
                         ff2m->ff_inv(ff2m, mx->coeff[mx->degree]));
        j = ax->degree - mx->degree;
        for (i=0; i<mx->degree; ++i,++j) {
            if (mx->coeff[i])
                ax->coeff[j] = ff2m->ff_add(ff2m, ax->coeff[j],
                                            ff2m->ff_mul(ff2m, mx->coeff[i], a));
        }
        ax->coeff[j] = 0;
        while ((ax->degree >= 0) && !ax->coeff[ax->degree])
            --ax->degree;
    }
}

int gcd_poly(const FF2m* ff2m, const poly* ax, const poly *bx, poly *gx)
{
    poly *sx, *tx;
    
    sx = clone_poly(ax);
    tx = clone_poly(bx);
    if (!sx || !tx)
        return 0;
    
    while (tx->degree >= 0) {
        /* g(x) = s(x) */
        COPY_POLY(sx, gx);
        
        /* g(x) = g(x) mod t(x) */
        modulo_reduce_poly(ff2m, tx, gx);
        
        /* s(x) = t(x) */
        COPY_POLY(tx, sx);
        
        /* t(x) = g(x) */
        COPY_POLY(gx, tx);
    }
    COPY_POLY(sx, gx);
    
    free_poly(sx);
    free_poly(tx);
    
    return 1;
}

poly* poly_from_roots(const FF2m* ff2m, const ff_unit* roots, size_t root_size)
{
    int i = 0, j;
    poly* ax = init_poly((int)root_size + 1);
    
    ax->coeff[0] = roots[i];
    ax->coeff[1] = 1;
    ax->degree = 1;
    
    for (i=1; i<(int)root_size; i++) {
        ax->coeff[ax->degree+1] = ax->coeff[ax->degree];
        for (j=ax->degree-1; j>=0; j--) {
            ax->coeff[j+1] = ff2m->ff_add(ff2m, ax->coeff[j],
                                          ff2m->ff_mul(ff2m, ax->coeff[j+1], roots[i]));
        }
        ax->coeff[0] = ff2m->ff_mul(ff2m, ax->coeff[0], roots[i]);
        
        ax->degree++;
        update_poly_degree(ax);
    }
    
    return ax;
}

/**
 *  Fast constant-time gcd based on:
 *  Daniel J. Bernstein and Bo-Yin Yang, Fast constant-time gcd computation
 *  and modular inversion, https://eprint.iacr.org/2019/266 
 **/
#define IMPLIES(a,b) (!(a) || (b))

void CT_update_poly_degree(poly *px, int degree)
{
    px->degree = degree;
    int zero = 1;
    for(int i = degree; i >= 0; i--){
        zero &= CT_is_equal_zero(px->coeff[i]);
        px->degree -= zero;
    }  
}

/**
 * returns -1 to indicate failure
 **/
int divstepsx(const FF2m* ff2m, int n, int t, int delta, const poly* fx, const poly* gx, poly** ff, poly ** gg) {
    poly* g2 = init_poly((1 << ff2m->m));
    poly* f2 = init_poly((1 << ff2m->m));
    uint16_t f0, g0;
    uint32_t sz = 0;
    *ff = clone_poly(fx);
    *gg = clone_poly(gx);
    if (!g2 || !f2 || !*ff || !*gg) {
        delta = -1;
        if (*ff) zero_poly(*ff);
        if (*gg) zero_poly(*gg);
        free_poly(*ff);
        free_poly(*gg);
        goto cleanup;
    }
    for (int i = 0; i < n; i++){
        COPY_POLY(*ff, f2);
        COPY_POLY(*gg, g2);

        sz = CT_is_less_than_zero(-delta) & CT_not(CT_is_equal_zero(g2->coeff[0]));
        
        delta = (1 + delta) - ((delta<<1)&(-sz));
        
        (*ff)->degree = (f2->degree) ^ ((-sz) & ((f2->degree) ^ (g2->degree)));
        for (int j = 0; j < f2->size; j++) {
            (*ff)->coeff[j] = (f2->coeff[j]) ^ ((-sz) & ((f2->coeff[j]) ^ (g2->coeff[j])));
        }
        f0 = f2->coeff[0];
        g0 = g2->coeff[0];
        for(int i = 0; i < (*gg)->size-1; i++) {
            (*gg)->coeff[i] = ff2m->ff_add(ff2m, ff2m->ff_mul(ff2m,f0, g2->coeff[i+1]), ff2m->ff_mul(ff2m, g0, f2->coeff[i+1]));
        }
        CT_update_poly_degree((*gg), t);
    }
cleanup:
    if (f2) zero_poly(f2);
    if (g2) zero_poly(g2);
    free_poly(f2);
    free_poly(g2);
    return delta;
}

poly* reverse(const FF2m* ff2m, int degree, const poly* ax) {
    poly* f = init_poly((1 << ff2m->m));
    if (!f) {
        return NULL;
    }  
    int zero = 1;
    int d = degree;
    for (int i = 0; i < d+1; i++) {
        f->coeff[i] = ax->coeff[d - i];
    }
    f->degree = d;
    for (int i = f->degree; i >= 0; i--) {
        zero &= CT_is_equal_zero(f->coeff[i]);
        f->degree -= zero;
    }
    return f;
}

/**
 * Assumes deg(ax)>deg(bx)
 **/
int CT_gcd_degree(const FF2m* ff2m, const poly* ax, const poly *bx) {
    poly *ff, *gg, *f, *g;
    int delta = -1, d = ax->degree;
    f = reverse(ff2m, d, ax);
    g = reverse(ff2m, d-1, bx);
    if (!f || !g) {
        goto cleanup;
    }
    delta = divstepsx(ff2m, 2*d-1, 2*d-1, 1, f, g, &ff, &gg);
    delta = CT_mux(CT_is_greater_than_or_equal_to_zero(delta), delta >> 1, delta);

cleanup:
    if (g) zero_poly(g);
    if (f) zero_poly(f);
    if (gg) zero_poly(gg);
    if (ff) zero_poly(ff);
    free_poly(g);
    free_poly(f);
    free_poly(gg);
    free_poly(ff);
    return delta;
}

/**
 * Assumes deg(ax)>deg(bx)
 **/
int CT_gcd_poly(const FF2m* ff2m, const poly* ax, const poly *bx, poly *gx) {
    poly *ff, *gg, *f, *g, *h = NULL;
    uint16_t f0;
    int delta = -1, d = ax->degree;
    f = reverse(ff2m, d, ax);
    g = reverse(ff2m, d-1, bx);
    if (!f || !g) {
        goto cleanup;
    }
    delta = divstepsx(ff2m, 2*d-1, 3*d-1, 1, f, g, &ff, &gg);
    if (CT_is_less_than_zero(delta)) {
        goto cleanup;
    }
    f0 =ff2m->ff_inv(ff2m, ff->coeff[0]);
    h = reverse(ff2m, delta>>1, ff);
    if (!h) {
        delta = -1;
        goto cleanup;
    }
    COPY_POLY(h, gx);
    for (int i = 0; i < d; i++) {
        gx->coeff[i] = ff2m->ff_mul(ff2m, gx->coeff[i], f0);
    }

cleanup:
    free_poly(h);
    free_poly(g);
    free_poly(f);
    free_poly(gg);
    free_poly(ff);
    return CT_is_greater_than_or_equal_to_zero(delta);
}
