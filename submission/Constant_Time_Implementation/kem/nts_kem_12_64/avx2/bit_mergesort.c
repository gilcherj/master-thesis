/**
 *  bit_mergesort.c
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(12, 64)
 *  Platform: Intel 64-bit
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#include <stdint.h>
#include "bit_mergesort_masks.h"
#include "bit_mergesort.h"
#include "nts_kem_params.h"
#include "bits.h"

#define DEGREE      NTS_KEM_PARAM_M
#define NETSIZE     NTS_KEM_PARAM_N
#define STRIDE      (NETSIZE >> 6)

#define POW2(n) (1U<<(n))
#define GET_SLICE(control, stage, step)     ((control)+((stage)*DEGREE*STRIDE)+((step)*STRIDE))

void compareCB(uint16_t* key, uint64_t* control, uint32_t i, uint32_t j, uint32_t offset, int stage, int step);
static inline void sort_layer(uint16_t* key, uint64_t* control, uint32_t n, uint32_t offset);
static inline void compareCB_avx(uint16_t* key, uint64_t* control, uint32_t i, uint32_t j, uint32_t offset, int stage, int step);
static inline void merge_layer_avx(uint16_t* key, uint64_t* control, uint32_t n, uint32_t offset, int stage);
static inline void merge_layer(uint16_t* in, uint64_t* control, uint32_t n, uint32_t offset, int stage);

/**
 *  Computes swaps required to apply a permutation.
 *  
 *  @param[in,out]  in          Permutation, will be sorted on return
 *  @param[in]      n           Degree of the Network
 *  @param[out]     control     Swaps required to apply the permutation 
 **/
void computeControlBits_avx(uint16_t* in, uint32_t n, uint64_t* control) {
    uint32_t i, l;
    l = n < 5 ? n : 5;
    for (i = 1; i <= l; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=32){
            sort_layer(in, control, i-1, j);
        }
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_layer(in, control, i, j, i-1);
        }
    }
    for (; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)){
            for (uint32_t k = 0; k < POW2(i-1U) ; k+= 16){
                sort_layer(in, control, i-1, j+k);
            }
        }
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_layer_avx(in, control, i, j, i-1);
        }
    }
}

/**
 *  Apply inverse of the permutation defined via control mask to a bit vector.
 *  
 *  @param[in,out]  arr         bit vector to be permuted
 *  @param[out]     control     Swaps defining the permutation 
 **/
void bitPermuteInv(uint64_t* arr, const uint64_t* control) {
    uint64_t l, r, shift, cshift, t, stride, slice;
    int64_t stage, step;
    stage = DEGREE-1;
#if DEGREE > 12
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice += 128) {
        for (step = stage; step >= 7; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;

            for (stride = 0; stride < 127; stride++) {
                l = arr[slice + stride + 0] & (left_compressed[step-6][0] ^ left_compressed[step - 6][1]);
                r = arr[slice + stride + 1] & (right_compressed[step-6][1] ^ right_compressed[step - 6][0]);

                arr[slice + stride + 0] ^= l;
                arr[slice + stride + 1] ^= r;

                t = (l >> cshift) ^ r;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t;
                l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t) << cshift;

                arr[slice + stride + 0] |= l;
                arr[slice + stride + 1] |= r;
            }

            if (cshift != shift) {
                for (stride = 0; stride < 128; stride++) {

                    l = arr[slice + stride] & left_compressed[step - 6][1];
                    r = arr[slice + stride] & right_compressed[step - 6][0];
                    arr[slice + stride] ^= l | r;

                    t = (r >> shift) ^ l;
                    l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                    r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                    arr[slice + stride] |= l | r;
                }
            }
        }

        for (uint64_t j = 2; j < 64; j <<= 1) {
            for (stride = 0; stride < (128UL - j); stride += j) {
                uint64_t dist = j >> 1;
                for (uint64_t i = dist; i < j; i++) {
                    l = arr[slice + stride + i] & left_compressed[0][0];
                    r = arr[slice + stride + i + dist] & right_compressed[0][1];
                    arr[slice + stride + i] ^= l;
                    arr[slice + stride + i + dist] ^= r;

                    t = l ^ r;
                    l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                    r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                    arr[slice + stride + i] |= l;
                    arr[slice + stride + i + dist] |= r;
                }
            }
            step--;
        }

        for (stride = 0; stride < 32; stride++) {
            l = arr[slice + stride + 32] & left_compressed[0][0];
            r = arr[slice + stride + 64] & right_compressed[0][1];
            arr[slice + stride + 32] ^= l;
            arr[slice + stride + 64] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 32] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 32] & t;

            arr[slice + stride + 32] |= l;
            arr[slice + stride + 64] |= r;
        }
        step--;

        for (stride = 0; stride < 64; stride++) {
            l = arr[slice + stride + 0] & left_compressed[0][0];
            r = arr[slice + stride + 64] & right_compressed[0][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 64] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[stride + slice] & t;
            r ^= GET_SLICE(control, stage, step)[stride + slice] & t;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 64] |= r;
        }
        step--;
    }
    stage--;
#endif
#if DEGREE > 11
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice += 64) {
        for (step = stage; step >= 7; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;
            for (stride = 0; stride < 64; stride++) {
                l = arr[slice + stride] & left_compressed[step - 5][1];
                r = arr[slice + stride] & right_compressed[step - 5][0];
                arr[slice + stride] ^= l | r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                arr[slice + stride] |= l | r;
            }

            for (stride = 0; stride < 63; stride++) {
                l = arr[slice + stride + 0] & (left_compressed[step - 5][0] ^ left_compressed[step - 5][1]);
                r = arr[slice + stride + 1] & (right_compressed[step - 5][1] ^ right_compressed[step - 5][0]);

                arr[slice + stride + 0] ^= l;
                arr[slice + stride + 1] ^= r;

                t = (l >> cshift) ^ r;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t;
                l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t) << cshift;

                arr[slice + stride + 0] |= l;
                arr[slice + stride + 1] |= r;
            }
        }

        shift = 1 << (stage - step);
        for (stride = 0; stride < 63; stride++) {
            l = arr[slice + stride + 0] & left_compressed[step - 5][0];
            r = arr[slice + stride + 1] & right_compressed[step - 5][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 1] ^= r;

            t = (l >> shift) ^ r;
            r ^= (GET_SLICE(control, stage, step)[slice + stride] >> shift)& t;
            l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> shift)& t) << shift;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 1] |= r;
        }

        step--;

        for (stride = 0; stride < 61; stride += 2) {
            l = arr[slice + stride + 1] & left_compressed[0][0];
            r = arr[slice + stride + 2] & right_compressed[0][1];
            arr[slice + stride + 1] ^= l;
            arr[slice + stride + 2] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;

            arr[slice + stride + 1] |= l;
            arr[slice + stride + 2] |= r;
        }
        step--;

        for (stride = 0; stride < 60; stride += 4) {
            for (uint64_t i = 2; i < 4; i++) {
                l = arr[slice + stride + i] & left_compressed[0][0];
                r = arr[slice + stride + i + 2] & right_compressed[0][1];
                arr[slice + stride + i] ^= l;
                arr[slice + stride + i + 2] ^= r;

                t = l ^ r;
                l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                arr[slice + stride + i] |= l;
                arr[slice + stride + i + 2] |= r;
            }
        }
        step--;

        for (stride = 0; stride < 56; stride += 8) {
            for (uint64_t i = 4; i < 8; i++) {
                l = arr[slice + stride + i] & left_compressed[0][0];
                r = arr[slice + stride + i + 4] & right_compressed[0][1];
                arr[slice + stride + i] ^= l;
                arr[slice + stride + i + 4] ^= r;

                t = l ^ r;
                l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                arr[slice + stride + i] |= l;
                arr[slice + stride + i + 4] |= r;
            }
        }
        step--;

        for (stride = 0; stride < 48; stride += 16) {
            for (uint64_t i = 8; i < 16; i++) {
                l = arr[slice + stride + i] & left_compressed[0][0];
                r = arr[slice + stride + i + 8] & right_compressed[0][1];
                arr[slice + stride + i] ^= l;
                arr[slice + stride + i + 8] ^= r;

                t = l ^ r;
                l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                arr[slice + stride + i] |= l;
                arr[slice + stride + i + 8] |= r;
            }
        }
        step--;

        for (stride = 0; stride < 16; stride++) {
            l = arr[slice + stride + 16] & left_compressed[0][0];
            r = arr[slice + stride + 32] & right_compressed[0][1];
            arr[slice + stride + 16] ^= l;
            arr[slice + stride + 32] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 16] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 16] & t;

            arr[slice + stride + 16] |= l;
            arr[slice + stride + 32] |= r;
        }
        step--;
        
        for (stride = 0; stride < 32; stride++) {
            l = arr[slice + stride + 0] & left_compressed[0][0];
            r = arr[slice + stride + 32] & right_compressed[0][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 32] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[stride + slice] & t;
            r ^= GET_SLICE(control, stage, step)[stride + slice] & t;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 32] |= r;
        }
        step--;
    }
    stage--;
#endif
#if DEGREE > 10
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice += 32) {
        for (step = stage; step >= 6; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;
            for (stride = 0; stride < 32; stride++) {

                l = arr[slice + stride] & left_compressed[step - 4][1];
                r = arr[slice + stride] & right_compressed[step - 4][0];
                arr[slice + stride] ^= l | r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                arr[slice + stride] |= l | r;
            }

            for (stride = 0; stride < 31; stride++) {
                l = arr[slice + stride + 0] & (left_compressed[step - 4][0] ^ left_compressed[step - 4][1]);
                r = arr[slice + stride + 1] & (right_compressed[step - 4][1] ^ right_compressed[step - 4][0]);

                arr[slice + stride + 0] ^= l;
                arr[slice + stride + 1] ^= r;

                t = (l >> cshift) ^ r;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t;
                l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t) << cshift;

                arr[slice + stride + 0] |= l;
                arr[slice + stride + 1] |= r;
            }
        }

        shift = 1 << (stage - step);
        for (stride = 0; stride < 31; stride++) {
            l = arr[slice + stride + 0] & left_compressed[step - 4][0];
            r = arr[slice + stride + 1] & right_compressed[step - 4][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 1] ^= r;

            t = (l >> shift) ^ r;
            r ^= (GET_SLICE(control, stage, step)[slice + stride] >> shift)& t;
            l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> shift)& t) << shift;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 1] |= r;
        }

        step--;

        for (stride = 0; stride < 29; stride += 2) {
            l = arr[slice + stride + 1] & left_compressed[0][0];
            r = arr[slice + stride + 2] & right_compressed[0][1];
            arr[slice + stride + 1] ^= l;
            arr[slice + stride + 2] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;

            arr[slice + stride + 1] |= l;
            arr[slice + stride + 2] |= r;
        }
        step--;

        for (stride = 0; stride < 25; stride += 4) {
            for (uint64_t i = 2; i < 4; i++) {
                l = arr[slice + stride + i] & left_compressed[0][0];
                r = arr[slice + stride + i + 2] & right_compressed[0][1];
                arr[slice + stride + i] ^= l;
                arr[slice + stride + i + 2] ^= r;

                t = l ^ r;
                l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                arr[slice + stride + i] |= l;
                arr[slice + stride + i + 2] |= r;
            }
        }
        step--;

        for (stride = 0; stride < 17; stride += 8) {
            for (uint64_t i = 4; i < 8; i++) {
                l = arr[slice + stride + i] & left_compressed[0][0];
                r = arr[slice + stride + i + 4] & right_compressed[0][1];
                arr[slice + stride + i] ^= l;
                arr[slice + stride + i + 4] ^= r;

                t = l ^ r;
                l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                arr[slice + stride + i] |= l;
                arr[slice + stride + i + 4] |= r;
            }
        }
        step--;

        for (stride = 0; stride < 8; stride++) {
            l = arr[slice + stride + 8] & left_compressed[0][0];
            r = arr[slice + stride + 16] & right_compressed[0][1];
            arr[slice + stride + 8] ^= l;
            arr[slice + stride + 16] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 8] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 8] & t;

            arr[slice + stride + 8] |= l;
            arr[slice + stride + 16] |= r;
        }
        step--;

        for (stride = 0; stride < 16; stride++) {
            l = arr[slice + stride + 0] & left_compressed[0][0];
            r = arr[slice + stride + 16] & right_compressed[0][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 16] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[stride + slice] & t;
            r ^= GET_SLICE(control, stage, step)[stride + slice] & t;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 16] |= r;
        }
        step--;
    }
    stage--;
#endif
#if DEGREE > 9
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice += 16) {
        for (step = stage; step >= 5; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;
            for (stride = 0; stride < 16; stride++) {

                l = arr[slice + stride] & left_compressed[step - 3][1];
                r = arr[slice + stride] & right_compressed[step - 3][0];
                arr[slice + stride] ^= l | r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                arr[slice + stride] |= l | r;
            }

            for (stride = 0; stride < 15; stride++) {
                l = arr[slice + stride + 0] & (left_compressed[step - 3][0] ^ left_compressed[step - 3][1]);
                r = arr[slice + stride + 1] & (right_compressed[step - 3][1] ^ right_compressed[step - 3][0]);

                arr[slice + stride + 0] ^= l;
                arr[slice + stride + 1] ^= r;

                t = (l >> cshift) ^ r;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t;
                l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t) << cshift;

                arr[slice + stride + 0] |= l;
                arr[slice + stride + 1] |= r;
            }
        }

        shift = 1 << (stage - step);
        for (stride = 0; stride < 15; stride++) {
            l = arr[slice + stride + 0] & left_compressed[step - 3][0];
            r = arr[slice + stride + 1] & right_compressed[step - 3][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 1] ^= r;

            t = (l >> shift) ^ r;
            r ^= (GET_SLICE(control, stage, step)[slice + stride] >> shift)& t;
            l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> shift)& t) << shift;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 1] |= r;
        }

        step--;

        for (stride = 0; stride < 13; stride += 2) {
            l = arr[slice + stride + 1] & left_compressed[0][0];
            r = arr[slice + stride + 2] & right_compressed[0][1];
            arr[slice + stride + 1] ^= l;
            arr[slice + stride + 2] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;

            arr[slice + stride + 1] |= l;
            arr[slice + stride + 2] |= r;
        }
        step--;

        for (stride = 0; stride < 9; stride+=4) {
            for (uint64_t i = 2; i < 4; i++) {
                l = arr[slice + stride + i] & left_compressed[0][0];
                r = arr[slice + stride + i + 2] & right_compressed[0][1];
                arr[slice + stride + i] ^= l;
                arr[slice + stride + i + 2] ^= r;

                t = l ^ r;
                l ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;
                r ^= GET_SLICE(control, stage, step)[slice + stride + i] & t;

                arr[slice + stride + i] |= l;
                arr[slice + stride + i + 2] |= r;
            }
        }
        step--;

        for (stride = 0; stride < 4; stride++) {
            l = arr[slice + stride + 4] & left_compressed[0][0];
            r = arr[slice + stride + 8] & right_compressed[0][1];
            arr[slice + stride + 4] ^= l;
            arr[slice + stride + 8] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 4] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 4] & t;

            arr[slice + stride + 4] |= l;
            arr[slice + stride + 8] |= r;
        }
        step--;

        for (stride = 0; stride < 8; stride++) {
            l = arr[slice + stride + 0] & left_compressed[0][0];
            r = arr[slice + stride + 8] & right_compressed[0][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 8] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[stride + slice] & t;
            r ^= GET_SLICE(control, stage, step)[stride + slice] & t;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 8] |= r;
        }
        step--;
    }
    stage--;
#endif
#if DEGREE > 8
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice += 8) {
        for (step = stage; step >= 4; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;
            for (stride = 0; stride < 8; stride++) {

                l = arr[slice + stride] & left_compressed[step - 2][1];
                r = arr[slice + stride] & right_compressed[step - 2][0];
                arr[slice + stride] ^= l | r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                arr[slice + stride] |= l | r;
            }

            for (stride = 0; stride < 7; stride++) {
                l = arr[slice + stride + 0] & (left_compressed[step - 2][0] ^ left_compressed[step - 2][1]);
                r = arr[slice + stride + 1] & (right_compressed[step - 2][1] ^ right_compressed[step - 2][0]);

                arr[slice + stride + 0] ^= l;
                arr[slice + stride + 1] ^= r;

                t = (l >> cshift) ^ r;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t;
                l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t) << cshift;

                arr[slice + stride + 0] |= l;
                arr[slice + stride + 1] |= r;
            }
        }

        shift = 1 << (stage - step);
        for (stride = 0; stride < 7; stride++) {
            l = arr[slice + stride + 0] & left_compressed[step - 2][0];
            r = arr[slice + stride + 1] & right_compressed[step - 2][1];

            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 1] ^= r;

            t = (l >> shift) ^ r;
            r ^= (GET_SLICE(control, stage, step)[slice + stride] >> shift)& t;
            l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> shift)& t) << shift;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 1] |= r;
        }
        step--;

        for (stride = 0; stride < 5; stride+=2) {
            l = arr[slice + stride + 1] & left_compressed[0][0];
            r = arr[slice + stride + 2] & right_compressed[0][1];
            arr[slice + stride + 1] ^= l;
            arr[slice + stride + 2] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 1] & t;

            arr[slice + stride + 1] |= l;
            arr[slice + stride + 2] |= r;
        }
        step--;

        for (stride = 0; stride < 2; stride++) {
            l = arr[slice + stride + 2] & left_compressed[0][0];
            r = arr[slice + stride + 4] & right_compressed[0][1];
            arr[slice + stride + 2] ^= l;
            arr[slice + stride + 4] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[slice + stride + 2] & t;
            r ^= GET_SLICE(control, stage, step)[slice + stride + 2] & t;

            arr[slice + stride + 2] |= l;
            arr[slice + stride + 4] |= r;
        }
        step--;

        for (stride = 0; stride < 4; stride++) {
            l = arr[slice + stride + 0] & left_compressed[0][0];
            r = arr[slice + stride + 4] & right_compressed[0][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 4] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[stride + slice] & t;
            r ^= GET_SLICE(control, stage, step)[stride + slice] & t;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 4] |= r;
        }
        step--;
    }
    stage--;
#endif
#if DEGREE > 7
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice += 4) {
        for (step = stage; step >= 3; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;
            for (stride = 0; stride < 4; stride++) {

                l = arr[slice + stride] & left_compressed[step - 1][1];
                r = arr[slice + stride] & right_compressed[step - 1][0];
                arr[slice + stride] ^= l | r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                arr[slice + stride] |= l | r;
            }

            for (stride = 0; stride < 3; stride++) {
                l = arr[slice + stride + 0] & (left_compressed[step - 1][0] ^ left_compressed[step - 1][1]);
                r = arr[slice + stride + 1] & (right_compressed[step - 1][1] ^ right_compressed[step - 1][0]);

                arr[slice + stride + 0] ^= l;
                arr[slice + stride + 1] ^= r;

                t = (l >> cshift) ^ r;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t;
                l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> cshift)& t) << cshift;

                arr[slice + stride + 0] |= l;
                arr[slice + stride + 1] |= r;
            }
        }

        shift = 1 << (stage - step);
        for (stride = 0; stride < 3; stride++) {
            l = arr[slice + stride + 0] & left_compressed[step - 1][0];
            r = arr[slice + stride + 1] & right_compressed[step - 1][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 1] ^= r;

            t = (l >> shift) ^ r;
            r ^= (GET_SLICE(control, stage, step)[slice + stride] >> shift)& t;
            l ^= ((GET_SLICE(control, stage, step)[slice + stride] >> shift)& t) << shift;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 1] |= r;
        }

        step--;

        l = arr[slice + 1] & left_compressed[0][0];
        r = arr[slice + 2] & right_compressed[0][1];
        arr[slice + 1] ^= l;
        arr[slice + 2] ^= r;

        t = l ^ r;
        l ^= GET_SLICE(control, stage, step)[slice + 1] & t;
        r ^= GET_SLICE(control, stage, step)[slice + 1] & t;

        arr[slice + 1] |= l;
        arr[slice + 2] |= r;

        step--;

        for (stride = 0; stride < 2; stride++) {
            l = arr[slice + stride + 0] & left_compressed[0][0];
            r = arr[slice + stride + 2] & right_compressed[0][1];
            arr[slice + stride + 0] ^= l;
            arr[slice + stride + 2] ^= r;

            t = l ^ r;
            l ^= GET_SLICE(control, stage, step)[stride + slice] & t;
            r ^= GET_SLICE(control, stage, step)[stride + slice] & t;

            arr[slice + stride + 0] |= l;
            arr[slice + stride + 2] |= r;
        }
        step--;
    }
    stage--;
#endif
#if DEGREE > 6
    for (slice = 0; slice < (1 << (DEGREE - 6)); slice+=2) {
        step = 0;
        for (step = stage; step >= 2; step--) {
            shift = 1 << (stage - step);
            cshift = 64 - shift;
            for (stride = 0; stride < 2; stride++) {

                l = arr[slice + stride] & left_compressed[step][1];
                r = arr[slice + stride] & right_compressed[step][0];
                arr[slice + stride] ^= l | r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[slice + stride] & t;
                r ^= (GET_SLICE(control, stage, step)[slice + stride] & t) << shift;

                arr[slice + stride] |= l | r;
            }
            l = arr[slice + 0] & (left_compressed[step][0] ^ left_compressed[step][1]);
            r = arr[slice + 1] & (right_compressed[step][1] ^ right_compressed[step][0]);

            arr[slice + 0] ^= l;
            arr[slice + 1] ^= r;

            t = (l >> cshift) ^ r;
            r ^= (GET_SLICE(control, stage, step)[slice] >> cshift)& t;
            l ^= ((GET_SLICE(control, stage, step)[slice] >> cshift)& t) << cshift;

            arr[slice + 0] |= l;
            arr[slice + 1] |= r;
        }

        l = arr[slice + 0] & left_compressed[step][0];
        r = arr[slice + 1] & right_compressed[step][1];
        arr[slice + 0] ^= l;
        arr[slice + 1] ^= r;

        shift = 1 << (stage - step);

        t = (l >> shift) ^ r;
        r ^= (GET_SLICE(control, stage, step)[slice] >> shift) & t;
        l ^= ((GET_SLICE(control, stage, step)[slice] >> shift) & t) << shift;

        arr[slice + 0] |= l;
        arr[slice + 1] |= r;

        step--;

        l = arr[slice + 0] & left_compressed[0][0];
        r = arr[slice + 1] & right_compressed[0][1];
        arr[slice + 0] ^= l;
        arr[slice + 1] ^= r;

        t = l ^ r;
        l ^= GET_SLICE(control, stage, step)[slice] & t;
        r ^= GET_SLICE(control, stage, step)[slice] & t;

        arr[slice + 0] |= l;
        arr[slice + 1] |= r;

        step--;
    }
    stage--;
#endif

    for (stage = 5; stage >= 0 ; stage--){
        for (step = stage; step >= 0; step--) {
            shift = 1 << (stage - step);
            for (uint64_t stride = 0; stride < STRIDE; stride++) {
                l = arr[stride] & left64[stage][step];
                r = arr[stride] & right64[stage][step];

                arr[stride] ^= l|r;

                t = (r >> shift) ^ l;
                l ^= GET_SLICE(control, stage, step)[stride] & t;
                r ^= (GET_SLICE(control, stage, step)[stride] & t) << shift;
                
                arr[stride] |= l|r;
            }
        }
    }
}

void compareCB(uint16_t* key, uint64_t* control, uint32_t i, uint32_t j, uint32_t offset, int stage, int step) {
    uint64_t c = CT_is_greater_than(key[offset+i], key[offset+j]);
    uint16_t t_key = key[offset+i] ^ key[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    GET_SLICE(control, stage, step)[(offset + i) >> LOG2] |= (c << ((offset+i) & MOD));
}

static inline void sort_layer(uint16_t* key, uint64_t* control, uint32_t n, uint32_t offset) {
    vector k1_t, k2_t, c, mask, t_key;
    uint32_t step = n < 5 ? 16 : POW2(n);
    vector k1 = _mm256_load_si256((__m256i*)(key+offset));
    vector k2 = _mm256_load_si256((__m256i*)(key+offset+step));
    vector perm_32 = _mm256_set_epi32(7, 5, 3, 1, 6, 4, 2, 0);
    vector perm_32inv = _mm256_set_epi32(7, 3, 6, 2, 5, 1, 4, 0);
    uint64_t bits;

    switch (n) {
        case 0:
            k1 = _mm256_shufflelo_epi16(k1, 0xD8);
            k2 = _mm256_shufflelo_epi16(k2, 0xD8);

            k1 = _mm256_shufflehi_epi16(k1, 0xD8);
            k2 = _mm256_shufflehi_epi16(k2, 0xD8);
        case 1:
        case 2:
            switch (n)
            {
            case 0:
            case 1:
                k1 = _mm256_permutevar8x32_epi32(k1, perm_32);
                k2 = _mm256_permutevar8x32_epi32(k2, perm_32);
                break;
            case 2:
                k1 = _mm256_permute4x64_epi64(k1, 0xD8);
                k2 = _mm256_permute4x64_epi64(k2, 0xD8);
                break;
            default:
                break;
            }
        case 3:
            k1_t = _mm256_permute2x128_si256(k1, k2, 0x20);
            k2_t = _mm256_permute2x128_si256(k1, k2, 0x31);
            c = _mm256_cmpgt_epi16(k1_t, k2_t);

            t_key = k1_t ^ k2_t;
            k1_t ^= c & t_key;
            k2_t ^= c & t_key;
            break;
        default:
            c = _mm256_cmpgt_epi16(k1, k2);

            t_key = k1 ^ k2;
            k1 ^= c & t_key;
            k2 ^= c & t_key;
            break;
    }

    if(n < 4) {

        k1 = _mm256_permute2x128_si256(k1_t, k2_t, 0x20);
        k2 = _mm256_permute2x128_si256(k1_t, k2_t, 0x31);

        if (n < 3) {
            if (n < 2) {
                k1 = _mm256_permutevar8x32_epi32(k1, perm_32inv);
                k2 = _mm256_permutevar8x32_epi32(k2, perm_32inv);
                if (n < 1) {
                    k1 = _mm256_shufflelo_epi16(k1, 0xD8);
                    k2 = _mm256_shufflelo_epi16(k2, 0xD8);

                    k1 = _mm256_shufflehi_epi16(k1, 0xD8);
                    k2 = _mm256_shufflehi_epi16(k2, 0xD8);
                    
                    mask = _mm256_setr_epi16(
                        0x1, 0x4, 0x10, 0x40, 0x100, 0x400, 0x1000, 0x4000,
                        0x1, 0x4, 0x10, 0x40, 0x100, 0x400, 0x1000, 0x4000
                        );
                } else {
                    mask = _mm256_setr_epi16(
                        0x1, 0x2, 0x10, 0x20, 0x100, 0x200, 0x1000, 0x2000,
                        0x1, 0x2, 0x10, 0x20, 0x100, 0x200, 0x1000, 0x2000
                        );
                }
            } else {
                k1 = _mm256_permute4x64_epi64(k1, 0xD8);
                k2 = _mm256_permute4x64_epi64(k2, 0xD8);

                mask = _mm256_setr_epi16(
                    0x1, 0x2, 0x4, 0x8, 0x100, 0x200, 0x400, 0x800,
                    0x1, 0x2, 0x4, 0x8, 0x100, 0x200, 0x400, 0x800
                    );
            }
        } else {
            mask = _mm256_setr_epi16(
                0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80,
                0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80
            );  
        }
        c &= mask;
        mask = _mm256_setzero_si256();
        c = _mm256_hadd_epi16(c, mask);
        c = _mm256_hadd_epi16(c, mask);
        c = _mm256_hadd_epi16(c, mask);
        bits = _mm256_extract_epi16(c, 0) + (_mm256_extract_epi16(c, 8) << 16);
        GET_SLICE(control, n, 0)[(offset) >> LOG2] |= (bits << ((offset) & MOD));
    } else {
        mask = _mm256_setr_epi16(
            0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80,
            0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, -0x8000
        );
        c &= mask;
        mask = _mm256_setzero_si256();
        c = _mm256_hadd_epi16(c, mask);
        c = _mm256_hadd_epi16(c, mask);
        c = _mm256_hadd_epi16(c, mask);
        bits = _mm256_extract_epi16(c, 0) + _mm256_extract_epi16(c, 8);

        GET_SLICE(control, n, 0)[(offset) >> LOG2] |= (bits << ((offset) & MOD));
    }
    _mm256_store_si256((__m256i*)(key+offset), k1);
    _mm256_store_si256((__m256i*)(key+offset+step), k2);
}

static inline void compareCB_avx(uint16_t* key, uint64_t* control, uint32_t i, uint32_t j, uint32_t offset, int stage, int step) {
    vector k1 = _mm256_load_si256((__m256i*)(key+offset+i));
    vector k2 = _mm256_load_si256((__m256i*)(key+offset+j));
    vector c = _mm256_cmpgt_epi16(k1, k2);

    vector t_key = k1 ^ k2;

    k1 ^= c & t_key;
    k2 ^= c & t_key;

    _mm256_store_si256((__m256i*)(key+offset+i), k1);
    _mm256_store_si256((__m256i*)(key+offset+j), k2);

    vector mask = _mm256_setr_epi16(
        0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80,
        0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, -0x8000
    );
    c &= mask;
    mask = _mm256_setzero_si256();
    c = _mm256_hadd_epi16(c, mask);
    c = _mm256_hadd_epi16(c, mask);
    c = _mm256_hadd_epi16(c, mask);
    uint64_t bits = _mm256_extract_epi16(c, 0) + _mm256_extract_epi16(c, 8);
    GET_SLICE(control, stage, step)[(offset + i) >> LOG2] |= (bits << ((offset+i) & MOD));
}

static inline void merge_layer_avx(uint16_t* key, uint64_t* control, uint32_t n, uint32_t offset, int stage) {
    uint32_t step;
    for (step = 1; step < n-4U; step++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-step)); j+=POW2(n-step)) {
            for (uint32_t i = j + POW2(n-step-1U); i < j + POW2(n-step) - 15U; i+=16) {
                compareCB_avx(key, control, i, i+POW2(n-step-1U), offset, stage, step);
            }
        }
    }
    for (; step < n; step++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-step)); j+=POW2(n-step)) {
            for (uint32_t i = j + POW2(n-step-1U); i < j + POW2(n-step); i++) {
                compareCB(key, control, i, i+POW2(n-step-1U), offset, stage, step);
            }
        }
    }
}

static inline void merge_layer(uint16_t* in, uint64_t* control, uint32_t n, uint32_t offset, int stage) {
    for (uint32_t step = 1; step < n; step++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-step)); j+=POW2(n-step)) {
            for (uint32_t i = j + POW2(n-step-1U); i < j + POW2(n-step); i++) {
                compareCB(in, control, i, i+POW2(n-step-1U), offset, stage, step);
            }
        }
    }
}

