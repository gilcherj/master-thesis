/**
 *  gauss_elimination.h
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(12, 64)
 *  Platform: AVX2
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#ifndef __GAUSS_ELIMINATION_H
#define __GAUSS_ELIMINATION_H

#include <stdint.h>
#include "matrix_ff2.h"

/**
 *  Transform matrix into row-reduced echelon form
 *
 *  @note
 *  The transformation is performed in-place.
 *  This implementation transforms the matrix from
 *  last column downwards, i.e. it tries to create
 *  an identity matrix on the right-hand side of
 *  matrix A.
 *
 *  @param[in,out] A    Matrix A
 *  @return The rank matrix A
 **/
uint32_t rref_rev(matrix_ff2* A);

/**
 *  Transform matrix into systematic form
 *
 *  @note
 *  The transformation is performed in-place.
 *  This implementation transforms the matrix from
 *  last column downwards, i.e. it tries to create
 *  an identity matrix on the right-hand side of
 *  matrix A.
 *
 *  @param[in,out] A    Matrix A
 *  @return The rank matrix A, -1 if systematic matrix can not
 *          be generated without columns swaps.
 **/
int32_t systematic_rev(matrix_ff2* A);
#endif
