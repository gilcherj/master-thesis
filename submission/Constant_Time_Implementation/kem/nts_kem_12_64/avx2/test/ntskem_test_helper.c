
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "nts_kem.h"
#include "ff.h"
#include "bits.h"
#include "matrix_ff2.h"
#include "polynomial.h"
#include "random.h"
#include "keccak.h"
#include "nts_kem_params.h"
#include "nts_kem_errors.h"
#include "vector_utils.h"
#include "ntskem_test_helper.h"

typedef struct {
    uint32_t m;
    FF2m *ff2m;
    ff_unit a[ NTS_KEM_PARAM_BC ];
    ff_unit h[ NTS_KEM_PARAM_BC ];
    ff_unit p[ NTS_KEM_PARAM_N ];
    uint8_t z[ NTS_KEM_KEY_SIZE ];
} NTSKEM_private;

static const int kNTSKEMKeysize = NTS_KEM_KEY_SIZE;

#define NTS_KEM_PARAM_A_REM     (((NTS_KEM_PARAM_A - (NTS_KEM_KEY_SIZE << 3)) & MOD) >> 3)
#define UINT64_SIZE             1
#define BLOCK_SIZE              64
#define NTS_KEM_PARAM_BC_VEC    NTS_KEM_PARAM_BC_DIV_64
#define NTS_KEM_PARAM_R_VEC     NTS_KEM_PARAM_R_DIV_64
#define NTS_KEM_PARAM_N_VEC     NTS_KEM_PARAM_N_DIV_64

/**
 *  Create a random vector `e` of length `n` bits with
 *  Hamming weight `tau`
 *
 *  @param[in]  tau  The desired Hamming weight
 *  @param[in]  n    The length of the sequence in bits
 *  @param[out] e    The output vector
 **/
void random_vector_test(uint32_t tau, uint32_t n, uint8_t *e)
{
    int32_t i;
    uint8_t a, b;
    ff_unit index;
    
    /**
     * Create a vector with `tau` non-zeros in
     * the last `tau` coordinates
     **/
    memset(e, 0, (n + 7) >> 3);
    for (i=0; i<tau >> 3; i++)
        e[(n>>3)-i-1] = 0xFF;
    
    /**
     * Shuffle the sequence e to randomise the location
     * of errors
     **/
    i = n-1;
    while (i >= n-tau) {
        index = random_uint16_bounded(i+1);
        a = (e[index >> 3] & (1 << (index & 7))) >> (index & 7);
        b = (e[i >> 3] & (1 << (i & 7))) >> (i & 7);
        e[index >> 3] &= ~(1 << (index & 7));
        e[index >> 3] |=  (b << (index & 7));
        e[i     >> 3] &= ~(1 << (i     & 7));
        e[i     >> 3] |=  (a << (i     & 7));
        --i;
    }
}

/**
 *  NTS-KEM encapsulation
 *
 *  @param[in]  pk      The pointer to NTS-KEM public key
 *  @param[in]  pk_size The size of the public key in bytes
 *  @param[in]  tau     The hemming weight of the error vector
 *  @param[out] c_ast   The pointer to the NTS-KEM ciphertext
 *  @param[out] k_r     The pointer to the encapsulated key
 *  @return NTS_KEM_SUCCESS on success, otherwise a negative error code
 *          {@see nts_kem_errors.h}
 **/
int nts_kem_test_encapsulate(const uint8_t *pk,
                        size_t pk_size,
                        uint32_t tau,
                        uint8_t *c_ast,
                        uint8_t *k_r)
{
    int status = NTS_KEM_BAD_MEMORY_ALLOCATION;
    int32_t i, j, l;
    packed_t v;
    const uint8_t *pk_ptr = pk;
    uint64_t c_c[NTS_KEM_PARAM_R_VEC];
    uint64_t Q[NTS_KEM_PARAM_K][NTS_KEM_PARAM_R_VEC];
    uint8_t kr_in_buf[kNTSKEMKeysize + NTS_KEM_PARAM_CEIL_N_BYTE];
    uint8_t k_e[kNTSKEMKeysize], e[NTS_KEM_PARAM_CEIL_N_BYTE];
   
    /* Extract information from public-key */
    
    if (kNTSKEMKeysize > (NTS_KEM_PARAM_K >> 3)) {
        return NTS_KEM_BAD_KEY_LENGTH;
    }
    
    /**
     * Populate the generator matrix, but only the parity section
     **/
    for (i=0; i<NTS_KEM_PARAM_K; i++) {
        Q[i][NTS_KEM_PARAM_R_VEC-1] = 0ULL;
        memcpy(Q[i], pk_ptr, NTS_KEM_PARAM_CEIL_R_BYTE);
        pk_ptr += NTS_KEM_PARAM_CEIL_R_BYTE;
    }

    /**
     * NTS-KEM Encapsulation procudure
     *
     * Step 1. Generate a uniformly distributed random vector e
     *         of length n and Hamming weight τ
     * Step 2. Partition e into sections, e = ( e_a | e_b | e_c )
     **/
    random_vector_test(tau, NTS_KEM_PARAM_N, e);

    /**
     * Step 3. Compute SHA3_256(e) to produce k_e
     **/
    sha3_256(e, NTS_KEM_PARAM_CEIL_N_BYTE, k_e);

    /**
     * Step 4. Construct a length k message vector m = (e_a | k_e)
     * Step 5. Perform systematic encoding with matrix Q,
     *         i.e. c = ( m | mQ ) + e
     *                = ( c_a | c_b | c_c )
     *                = ( 0_a | c_b | c_c )
     *          c_ast = ( c_b | c_c )
     *
     * The c_c (or parity block) is basically section c of the
     * ciphertext. It's basically c_c = (e_a | k_e)*Q where Q
     * is the last n-k bits of the generator matrix in reduced
     * echelon form G = [ I | Q ].
     *
     * Instead of doing matrix multiplication, we use vectorised
     * XOR operations.
     **/
    for (i=0; i<NTS_KEM_PARAM_R_VEC; i++)
        c_c[i] = 0ULL;
    for (i=0; i<NTS_KEM_PARAM_A >> LOG2; i++) {
        memcpy(&v, &e[i*sizeof(v)], sizeof(v));
        while (v) {
            l = (int32_t)lowest_bit_idx(v);
            v ^= (ONE << l);
            l += (BITSIZE*i);
            for (j=0; j<NTS_KEM_PARAM_R_VEC; j++) {
                c_c[j] ^= Q[l][j];
            }
        }
    }
    v = 0x0ULL;
    memcpy(&v,  &e[i*sizeof(v)], NTS_KEM_PARAM_A_REM);
    while (v) {
        l = (int32_t)lowest_bit_idx(v);
        v ^= (ONE << l);
        l += (BITSIZE*i);
        for (j=0; j<NTS_KEM_PARAM_R_VEC; j++) {
            c_c[j] ^= Q[l][j];
        }
    }
    for (i=0; i<NTS_KEM_PARAM_B >> LOG2; i++) {
        memcpy(&v, &k_e[i*sizeof(v)], sizeof(v));
        while (v) {
            l = (int32_t)lowest_bit_idx(v);
            v ^= (ONE << l);
            l += ((BITSIZE*i) + NTS_KEM_PARAM_A);
            for (j=0; j<NTS_KEM_PARAM_R_VEC; j++) {
                c_c[j] ^= Q[l][j];
            }
        }
    }

    /**
     * The output is ciphertext containing the following section:
     *
     *     c = ( c_a | c_b | c_c )
     *
     * By construction, c_b = k_e and its length is kNTSKEMKeysize bytes,
     * the length of c_a is (k/8 - kNTSKEMKeysize) bytes and the length of
     * c_c is (n-k)/8 bytes.
     *
     * The error pattern e = ( e_a | e_b | e_c ), therefore, after
     * adding e to c, c_a = 0, and we have
     *
     *     c_ast = ( k_e + e_b | c_c + e_c )
     **/
    memcpy(c_ast, k_e, kNTSKEMKeysize);                             /* k_e */
    memcpy(&c_ast[kNTSKEMKeysize], c_c, NTS_KEM_PARAM_CEIL_R_BYTE); /* c_c */
    
    /**
     * Perturb the NTS ciphertext with error pattern in section b and c.
     *
     * There is no need to perturb section a as we know it will result
     * to 0 and we are going to drop this section anyway.
     */
    for (i=0; i<NTS_KEM_CIPHERTEXT_SIZE; i++) {
        c_ast[i] ^= e[(NTS_KEM_PARAM_A>>3) + i];    /* c_b = k_e + e_b, c_c = c_c + e_c */
    }
    
    /**
     * Step 6. Output the pair (k_r, c_ast) where k_r = SHA3_256(k_e | e)
     *
     * Construct (k_e | e) and obtain k_r = SHA3_256(k_e | e)
     **/
    memcpy(kr_in_buf, k_e, kNTSKEMKeysize);
    memcpy(&kr_in_buf[kNTSKEMKeysize], e, NTS_KEM_PARAM_CEIL_N_BYTE);
    sha3_256(kr_in_buf, kNTSKEMKeysize + NTS_KEM_PARAM_CEIL_N_BYTE, k_r);
    

    status = NTS_KEM_SUCCESS;
    memset(e, 0, NTS_KEM_PARAM_CEIL_N_BYTE);
    memset(kr_in_buf, 0, kNTSKEMKeysize + NTS_KEM_PARAM_CEIL_N_BYTE);
    return status;
}
