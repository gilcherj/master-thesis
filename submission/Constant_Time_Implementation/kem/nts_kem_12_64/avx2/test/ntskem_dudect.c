#include <stdlib.h>
#include <stdint.h>
#include <string.h> // memcmp
#include <stdio.h>
#include "api.h"
#include "dut.h"
#include "random.h"

uint8_t *pk, *sk;
uint8_t *encap_key, *decap_key, *ciphertext;
unsigned char entropy_input[] = {
    0xaa, 0xe7, 0xd7, 0x4e, 0x3c, 0x3a, 0x52, 0xdd,
    0x87, 0xc7, 0x2a, 0xa4, 0x38, 0x54, 0x7e, 0x37,
    0x1e, 0x97, 0x29, 0x78, 0x22, 0xa2, 0xcd, 0x83,
    0x43, 0x64, 0x84, 0xcf, 0x77, 0x6b, 0x9e, 0xa5,
    0x53, 0xf3, 0x50, 0xc5, 0xc7, 0x8d, 0x46, 0xb3,
    0xa5, 0xf2, 0xe3, 0x99, 0x63, 0x10, 0x1d, 0x10
};
unsigned char nonce[48];

const size_t chunk_size = CRYPTO_CIPHERTEXTBYTES;
const size_t number_measurements = 1e3; // per test

uint8_t do_one_computation(uint8_t *data) {
    uint8_t ret = 0;

    crypto_kem_dec(decap_key, data, sk);

    ret ^= decap_key[0];
    return ret;
}

void init_dut(void) {
    int it = 0;
    FILE *fp = NULL;
    if ((fp = fopen("/dev/urandom", "r"))) {
        if ((sizeof(entropy_input) != fread(entropy_input, 1, sizeof(entropy_input), fp)) ||
            (sizeof(nonce) != fread(nonce, 1, sizeof(nonce), fp))) {
        }
    }
    fclose(fp);
    memcpy(&entropy_input[48-sizeof(it)], &it, sizeof(it));
    randombytes_init(entropy_input, nonce, 256);

    pk = (uint8_t *)calloc(CRYPTO_PUBLICKEYBYTES, sizeof(uint8_t));
    sk = (uint8_t *)calloc(CRYPTO_SECRETKEYBYTES, sizeof(uint8_t));
    crypto_kem_keypair(pk, sk);

    ciphertext = (uint8_t *)calloc(CRYPTO_CIPHERTEXTBYTES, sizeof(uint8_t));
    encap_key = (uint8_t *)calloc(CRYPTO_BYTES, sizeof(uint8_t));
    decap_key = (uint8_t *)calloc(CRYPTO_BYTES, sizeof(uint8_t));
}

void prepare_inputs(uint8_t *input_data, uint8_t *classes) {
    randombytes(input_data, number_measurements * chunk_size);
    for (size_t i = 0; i < number_measurements; i++) {
        classes[i] = randombit();
        if (classes[i] == 0) {
            crypto_kem_enc(ciphertext, encap_key, pk);
            memcpy(input_data + (size_t) i * chunk_size, pk, CRYPTO_CIPHERTEXTBYTES);
        } else {
            // leave random
        }
    }
}
