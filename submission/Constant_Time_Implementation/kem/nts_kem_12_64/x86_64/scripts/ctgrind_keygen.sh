#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
cd ..

for i in {0..3}; do
    mkdir -p log/O$i/
    make clean && make test_keygenO$i && valgrind --log-file=log/O$i/ctgrind_keygen.log --leak-check=full ./bin/ntskem-12-64-ct-ctgrind
done
make clean