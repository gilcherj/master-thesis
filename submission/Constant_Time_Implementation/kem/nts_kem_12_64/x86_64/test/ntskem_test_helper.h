#ifndef __NTSKEM_TEST_HELPER
#define __NTSKEM_TEST_HELPER

#include <stdint.h>

/**
 *  NTS-KEM encapsulation
 *
 *  @param[in]  pk      The pointer to NTS-KEM public key
 *  @param[in]  pk_size The size of the public key in bytes
 *  @param[in]  tau     The hemming weight of the error vector
 *  @param[out] c_ast   The pointer to the NTS-KEM ciphertext
 *  @param[out] k_r     The pointer to the encapsulated key
 *  @return NTS_KEM_SUCCESS on success, otherwise a negative error code
 *          {@see nts_kem_errors.h}
 **/
int nts_kem_test_encapsulate(const uint8_t *pk,
                        size_t pk_size,
                        uint32_t tau,
                        uint8_t *c_ast,
                        uint8_t *k_r);

#endif
