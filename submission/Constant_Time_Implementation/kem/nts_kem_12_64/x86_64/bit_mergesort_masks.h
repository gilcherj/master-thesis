/**
 *  bit_mergesort_masks.h
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(12, 64)
 *  Platform: Intel 64-bit
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#ifndef __BIT_MERGESORT_MASKS_H
#define __BIT_MERGESORT_MASKS_H
#include <stdint.h>


uint64_t left64[6][6] = {
        {
            0x5555555555555555, 0,
            0, 0,
            0, 0
        },
        {
            0x3333333333333333, 0x2222222222222222,
            0, 0,
            0, 0
        },
        {
            0x0F0F0F0F0F0F0F0F, 0x0C0C0C0C0C0C0C0C,
            0x2A2A2A2A2A2A2A2A, 0,
            0, 0
        },
        {
            0x00FF00FF00FF00FF, 0x00F000F000F000F0,
            0x0CCC0CCC0CCC0CCC, 0x2AAA2AAA2AAA2AAA,
            0, 0
        },
        {
            0x0000FFFF0000FFFF, 0x0000FF000000FF00,
            0x00F0F0F000F0F0F0, 0x0CCCCCCC0CCCCCCC,
            0x2AAAAAAA2AAAAAAA, 0
        },
        {
            0x00000000FFFFFFFF, 0x00000000FFFF0000,
            0x0000FF00FF00FF00, 0x00F0F0F0F0F0F0F0,
            0x0CCCCCCCCCCCCCCC, 0x2AAAAAAAAAAAAAAA
        },
};

uint64_t right64[6][6] = {
        {
            0xAAAAAAAAAAAAAAAA, 0,
            0, 0,
            0, 0
        },
        {
            0xCCCCCCCCCCCCCCCC, 0x4444444444444444,
            0, 0,
            0, 0
        },
        {
            0xF0F0F0F0F0F0F0F0, 0x3030303030303030,
            0x5454545454545454, 0,
            0, 0
        },
        {
            0XFF00FF00FF00FF00, 0x0F000F000F000F00,
            0x3330333033303330, 0x5554555455545554,
            0, 0
        },
        {
            0xFFFF0000FFFF0000, 0x00FF000000FF0000,
            0x0F0F0F000F0F0F00, 0x3333333033333330,
            0x5555555455555554, 0
        },
        {
            0xFFFFFFFF00000000, 0x0000FFFF00000000,
            0x00FF00FF00FF0000, 0x0F0F0F0F0F0F0F00,
            0x3333333333333330, 0x5555555555555554
        }
};

uint64_t left_compressed[7][2] = {
    {
        0xFFFFFFFFFFFFFFFF, 0x0000000000000000
    },
    {
        0xFFFFFFFF00000000, 0x0000000000000000
    },
    {
        0xFFFF0000FFFF0000, 0x00000000FFFF0000
    },
    {
        0xFF00FF00FF00FF00, 0x0000FF00FF00FF00
    },
    {
        0xF0F0F0F0F0F0F0F0, 0x00F0F0F0F0F0F0F0
    },
    {
        0xCCCCCCCCCCCCCCCC, 0x0CCCCCCCCCCCCCCC
    },
    {
        0xAAAAAAAAAAAAAAAA, 0x2AAAAAAAAAAAAAAA
    }
};

uint64_t right_compressed[7][2] = {
    {
        0x0000000000000000, 0xFFFFFFFFFFFFFFFF
    },
    {
        0x0000000000000000, 0x00000000FFFFFFFF
    },
    {
        0x0000FFFF00000000, 0x0000FFFF0000FFFF
    },
    {
        0x00FF00FF00FF0000, 0x00FF00FF00FF00FF
    },
    {
        0x0F0F0F0F0F0F0F00, 0x0F0F0F0F0F0F0F0F
    },
    {
        0x3333333333333330, 0x3333333333333333
    },
    {
        0x5555555555555554, 0x5555555555555555
    }
};

#endif
