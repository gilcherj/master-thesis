/**
 *  mergesort.h
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(13, 136)
 *  Platform: AVX2
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#ifndef __MERGESORT_H
#define __MERGESORT_H
#include <stdint.h>

/**
 *  Permute a vector in constant-time.
 *
 *  @param[in,out]  val     Vector to be permuted
 *  @param[in]      p       Permutation to be applied
 **/
void permute(uint16_t* val, const uint16_t* p);

/**
 *  Compute the inversion of a permutation in constant-time.
 *
 *  @param[in]      p       Permutation to be applied
 *  @param[out]     p_inv   Inverse permutation
 **/
void invert_permutation(const uint16_t* p, uint16_t* p_inv);

/**
 *  Create a uniformly random permutation in constant-time.
 *
 *  @param[out]      p       uniformly random permutation
 **/
void create_permutation(uint16_t* p);

#endif
