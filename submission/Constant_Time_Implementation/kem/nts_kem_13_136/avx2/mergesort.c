/**
 *  mergesort.c
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(13, 136)
 *  Platform:AVX2
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "mergesort.h"
#include "bits.h"
#include "nts_kem_errors.h"
#include "nts_kem_params.h"
#include "random.h"
#ifdef CTGRIND_KEYGEN
    #include "ctgrind.h"
#endif
#define NETSIZE (1 << NTS_KEM_PARAM_M)
#define POW2(n) (1U<<(n))

static inline void merge_layer(uint16_t* val, uint16_t* key, uint32_t n, uint32_t offset);
static inline void compare_key(uint16_t* val, uint16_t* key, uint32_t i, uint32_t j, uint32_t offset);
static inline void compare_key32(uint16_t* val, uint32_t* key, uint32_t i, uint32_t j, uint32_t offset);
static inline void merge_key32(uint16_t* val, uint32_t* key, uint32_t n, uint32_t offset);
static inline void merge_key64(uint16_t* val, uint64_t* key, uint32_t n, uint32_t offset);
static inline void compare_key64(uint16_t* val, uint64_t* key, uint32_t i, uint32_t j, uint32_t offset);
void mergesort_key_avx(uint16_t* val, uint16_t* key, uint32_t n);
void mergesort_key32(uint16_t* val, uint32_t* key, uint32_t n);
void mergesort_key64(uint16_t* val, uint64_t* key, uint32_t n) ;

/**
 *  Permute a vector in constant-time.
 *
 *  @param[in,out]  val     Vector to be permuted
 *  @param[in]      p       Permutation to be applied
 **/
void permute(uint16_t* val, const uint16_t* p) {
    uint16_t key[NETSIZE] __attribute__ ((aligned (32))) ;
    memcpy(key, p, NETSIZE*sizeof(uint16_t));
    mergesort_key_avx(val, key, NTS_KEM_PARAM_M);
}

/**
 *  Compute the inversion of a permutation in constant-time.
 *
 *  @param[in]      p       Permutation to be applied
 *  @param[out]     p_inv   Inverse permutation
 **/
void invert_permutation(const uint16_t* p, uint16_t* p_inv) {
    for (int i = 0; i < NETSIZE; i ++) {
        p_inv[i] = i;
    }
    permute(p_inv, p);
}

/**
 *  Create a uniformly random permutation in constant-time.
 *
 *  @param[out]      p       uniformly random permutation
 **/
void create_permutation(uint16_t* p) {
    uint64_t ran[NTS_KEM_PARAM_N];
    uint64_t t;
    do{
        t = 1;
        randombytes((uint8_t*) ran, sizeof(ran));
#ifdef CTGRIND_KEYGEN
        ct_poison(ran, sizeof(ran));
#endif
        for(int i = 0; i < NTS_KEM_PARAM_N; i++) {
            p[i] = i;
        }
        mergesort_key64(p, ran, NTS_KEM_PARAM_M);
        for(int i = 1; i < NTS_KEM_PARAM_N; i++) {
            t &= CT_is_not_equal64(ran[i], ran[i-1]);
        }
#ifdef CTGRIND_KEYGEN
    /*unpoison t, since it does not leak randomness, because we retry if t==0*/
    ct_unpoison(&t, sizeof(uint64_t));
#endif
    } while (CT_not64(t));
}

/** -------------------- Private methods -------------------- **/

void mergesort_key32(uint16_t* val, uint32_t* key, uint32_t n) {
    for (uint32_t i = 1; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_key32(val, key, i, j);
        }
    }
}

void mergesort_key64(uint16_t* val, uint64_t* key, uint32_t n) {
    for (uint32_t i = 1; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_key64(val, key, i, j);
        }
    }
}

static inline void compare_key(uint16_t* val, uint16_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    uint32_t c = CT_is_greater_than(key[offset+i], key[offset+j]);
    uint16_t t_key = key[offset+i] ^ key[offset+j];
    uint32_t t_val = val[offset+i] ^ val[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    val[offset+i] ^= (-c) & t_val;
    val[offset+j] ^= (-c) & t_val;
}

static inline void vector_compare_keys_and_cond_swap(vector *key1, vector *key2, vector *val1, vector *val2) {
    vector c = _mm256_cmpgt_epi16(*key1, *key2);

    vector t_key = *key1 ^ *key2;
    vector t_val = *val1 ^ *val2;
    *key1 ^= c & t_key;
    *key2 ^= c & t_key;
    *val1 ^= c & t_val;
    *val2 ^= c & t_val;
}

static inline void sort_layer(uint16_t* val, uint16_t* key, uint32_t n, uint32_t offset) {
    vector k1_t, k2_t, v1_t, v2_t;
    uint32_t step = n < 5 ? 16 : POW2(n);
    vector k1 = _mm256_load_si256((__m256i*)(key+offset));
    vector k2 = _mm256_load_si256((__m256i*)(key+offset+step));
    vector v1 = _mm256_load_si256((__m256i*)(val+offset));
    vector v2 = _mm256_load_si256((__m256i*)(val+offset+step));
    vector perm_32 = _mm256_set_epi32(7, 5, 3, 1, 6, 4, 2, 0);
    vector perm_32inv = _mm256_set_epi32(7, 3, 6, 2, 5, 1, 4, 0);

    switch (n) {
        case 0:
            k1 = _mm256_shufflelo_epi16(k1, 0xD8);
            k2 = _mm256_shufflelo_epi16(k2, 0xD8);
            v1 = _mm256_shufflelo_epi16(v1, 0xD8);
            v2 = _mm256_shufflelo_epi16(v2, 0xD8);

            k1 = _mm256_shufflehi_epi16(k1, 0xD8);
            k2 = _mm256_shufflehi_epi16(k2, 0xD8);
            v1 = _mm256_shufflehi_epi16(v1, 0xD8);
            v2 = _mm256_shufflehi_epi16(v2, 0xD8);
        case 1:
        case 2:
            switch (n)
            {
            case 0:
            case 1:
                k1 = _mm256_permutevar8x32_epi32(k1, perm_32);
                k2 = _mm256_permutevar8x32_epi32(k2, perm_32);
                v1 = _mm256_permutevar8x32_epi32(v1, perm_32);
                v2 = _mm256_permutevar8x32_epi32(v2, perm_32);
                break;
            case 2:
                k1 = _mm256_permute4x64_epi64(k1, 0xD8);
                k2 = _mm256_permute4x64_epi64(k2, 0xD8);
                v1 = _mm256_permute4x64_epi64(v1, 0xD8);
                v2 = _mm256_permute4x64_epi64(v2, 0xD8);
                break;
            default:
                break;
            }
        case 3:
            k1_t = _mm256_permute2x128_si256(k1, k2, 0x20);
            k2_t = _mm256_permute2x128_si256(k1, k2, 0x31);

            v1_t = _mm256_permute2x128_si256(v1, v2, 0x20);
            v2_t = _mm256_permute2x128_si256(v1, v2, 0x31);
            vector_compare_keys_and_cond_swap(&k1_t, &k2_t, &v1_t, &v2_t);
            break;
        default:
            vector_compare_keys_and_cond_swap(&k1, &k2, &v1, &v2);
            break;
    }

    if(n < 4) {

        k1 = _mm256_permute2x128_si256(k1_t, k2_t, 0x20);
        k2 = _mm256_permute2x128_si256(k1_t, k2_t, 0x31);

        v1 = _mm256_permute2x128_si256(v1_t, v2_t, 0x20);
        v2 = _mm256_permute2x128_si256(v1_t, v2_t, 0x31);

        if (n < 3) {
            if (n < 2) {
                k1 = _mm256_permutevar8x32_epi32(k1, perm_32inv);
                k2 = _mm256_permutevar8x32_epi32(k2, perm_32inv);
                v1 = _mm256_permutevar8x32_epi32(v1, perm_32inv);
                v2 = _mm256_permutevar8x32_epi32(v2, perm_32inv);
                if (n < 1) {
                    k1 = _mm256_shufflelo_epi16(k1, 0xD8);
                    k2 = _mm256_shufflelo_epi16(k2, 0xD8);
                    v1 = _mm256_shufflelo_epi16(v1, 0xD8);
                    v2 = _mm256_shufflelo_epi16(v2, 0xD8);

                    k1 = _mm256_shufflehi_epi16(k1, 0xD8);
                    k2 = _mm256_shufflehi_epi16(k2, 0xD8);
                    v1 = _mm256_shufflehi_epi16(v1, 0xD8);
                    v2 = _mm256_shufflehi_epi16(v2, 0xD8);
                }
            } else {
                k1 = _mm256_permute4x64_epi64(k1, 0xD8);
                k2 = _mm256_permute4x64_epi64(k2, 0xD8);
                v1 = _mm256_permute4x64_epi64(v1, 0xD8);
                v2 = _mm256_permute4x64_epi64(v2, 0xD8);
            }
        }
    }

    _mm256_store_si256((__m256i*)(key+offset), k1);
    _mm256_store_si256((__m256i*)(key+offset+step), k2);
    _mm256_store_si256((__m256i*)(val+offset), v1);
    _mm256_store_si256((__m256i*)(val+offset+step), v2);
}

static inline void compare_key_avx(uint16_t* val, uint16_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    vector k1 = _mm256_load_si256((__m256i*)(key+offset+i));
    vector k2 = _mm256_load_si256((__m256i*)(key+offset+j));
    vector v1 = _mm256_load_si256((__m256i*)(val+offset+i));
    vector v2 = _mm256_load_si256((__m256i*)(val+offset+j));

    vector_compare_keys_and_cond_swap(&k1, &k2, &v1, &v2);

    _mm256_store_si256((__m256i*)(key+offset+i), k1);
    _mm256_store_si256((__m256i*)(key+offset+j), k2);
    _mm256_store_si256((__m256i*)(val+offset+i), v1);
    _mm256_store_si256((__m256i*)(val+offset+j), v2);
}

static inline void merge_layer_avx(uint16_t* val, uint16_t* key, uint32_t n, uint32_t offset) {
    uint32_t k;
    for (k = 1; k < n-4U; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k) - 15U; i+=16) {
                compare_key_avx(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
    for (; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}
static inline void merge_layer(uint16_t* val, uint16_t* key, uint32_t n, uint32_t offset) {
    for (uint32_t k = 1; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}

void mergesort_key_avx(uint16_t* val, uint16_t* key, uint32_t n) {
    uint32_t i, l;
    l = n < 5 ? n : 5;
    for (i = 1; i <= l; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=32){
            sort_layer(val, key, i-1, j);
        }
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_layer(val, key, i, j);
        }
    }
    for (; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)){
            for (uint32_t k = 0; k < POW2(i-1U) ; k+= 16){
                sort_layer(val, key, i-1, j+k);
            }
        }
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_layer_avx(val, key, i, j);
        }
    }
}

static inline void merge_key32(uint16_t* val, uint32_t* key, uint32_t n, uint32_t offset) {
    for (uint32_t i = 0; i < POW2(n-1U); i++){
        compare_key32(val, key, i, i+POW2(n-1U), offset);
    }
    for (uint32_t k = 1; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key32(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}

static inline void compare_key32(uint16_t* val, uint32_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    uint32_t c = CT_is_greater_than(key[offset+i], key[offset+j]);
    uint32_t t_key = key[offset+i] ^ key[offset+j];
    uint16_t t_val = val[offset+i] ^ val[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    val[offset+i] ^= (-c) & t_val;
    val[offset+j] ^= (-c) & t_val;
}

static inline void compare_key64(uint16_t* val, uint64_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    uint64_t c = CT_is_greater_than64(key[offset+i], key[offset+j]);
    uint64_t t_key = key[offset+i] ^ key[offset+j];
    uint16_t t_val = val[offset+i] ^ val[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    val[offset+i] ^= (-c) & t_val;
    val[offset+j] ^= (-c) & t_val;
}

static inline void merge_key64(uint16_t* val, uint64_t* key, uint32_t n, uint32_t offset) {
    for (uint32_t i = 0; i < POW2(n-1U); i++){
        compare_key64(val, key, i, i+POW2(n-1U), offset);
    }
    for (uint32_t k = 1; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key64(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}
