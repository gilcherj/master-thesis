/**
 *  gauss_elimination.c
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(13, 136)
 *  Platform: AVX2
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "bits.h"
#include "nts_kem_errors.h"
#include "matrix_ff2.h"
#include "gauss_elimination.h"
#include "m4r.h"
#include "mem.h"
#ifdef CTGRIND_KEYGEN
    #include "ctgrind.h"
#endif
#define RREF_PACKED_BITS (sizeof(packed_t)*8)

/**
 *  Transform matrix into row-reduced echelon form
 *
 *  @note
 *  The transformation is performed in-place.
 *  This implementation transforms the matrix from
 *  last column downwards, i.e. it tries to create
 *  an identity matrix on the right-hand side of
 *  matrix A.
 *
 *  @param[in,out] A    Matrix A
 *  @return The rank matrix A
 **/
uint32_t rref_rev(matrix_ff2* A) {
    int64_t r, row, n;
    uint64_t found, c;
    packed_t m = 0;
    vector *v_ptr = NULL;
    vector mask = _mm256_setzero_si256(), tmp = _mm256_setzero_si256();
    found = 0;
    row = A->nrows - 1;

    vector row_ptr[A->stride/sizeof(vector)];
    memset(row_ptr, 0, sizeof(row_ptr));

    for (uint32_t col = A->ncols - 1; (int32_t)col >= 0; col--) {
        found = 0;

        for (r = A->nrows-1; r >= 0; r--) {
            v_ptr = (vector*) row_ptr_matrix_ff2(A, r);
            c = CT_is_equal(r, row);
            for(n = 0; n < A->stride/sizeof(vector); n++) {
                row_ptr[n] = CT_mux_vec(_mm256_set1_epi64x(c), v_ptr[n], row_ptr[n]);
            }
        }


        // search for pivot
        for (r = A->nrows-1; r >= 0; r--) {
            v_ptr = (vector*) row_ptr_matrix_ff2(A, r);
            // swap rows
            m = CT_is_less_than_or_equal_to(r, row);
            m &= CT_not(CT_is_less_than_zero(row));
            found |= bit_value((packed_t*) v_ptr, col) & m;
            m &= bit_value((packed_t*) v_ptr, col);
            mask = _mm256_set1_epi64x(-m);
            for (n = 0; n < A->stride/sizeof(vector); n++) {
                tmp = row_ptr[n] ^ v_ptr[n];
                row_ptr[n] ^= tmp & mask;
                v_ptr[n] ^= tmp & mask;
            }
        }

        // eliminate column
        for (r = 0; r < A->nrows; r++) {
            v_ptr = (vector*) row_ptr_matrix_ff2(A, r);
            m = bit_value((packed_t*) v_ptr, col);
            m &= CT_not(CT_is_less_than_zero(row));
            mask = _mm256_set1_epi64x(-m);
            c = CT_is_equal(row, r);
            for(n = 0; n < A->stride/sizeof(vector); n++) {
                tmp = v_ptr[n] ^ row_ptr[n];
                tmp = CT_mux_vec(_mm256_set1_epi64x(c), tmp, mask & row_ptr[n]);
                v_ptr[n] ^= tmp;
            }
        }
        row -= found;
    }
    CT_memset(row_ptr, 0, sizeof(row_ptr));
    return A->nrows - 1 - row;
}

/**
 *  Transform matrix into systematic form
 *
 *  @note
 *  The transformation is performed in-place.
 *  This implementation transforms the matrix from
 *  last column downwards, i.e. it tries to create
 *  an identity matrix on the right-hand side of
 *  matrix A.
 *
 *  @param[in,out] A    Matrix A
 *  @return The rank matrix A, -1 if systematic matrix can not
 *          be generated without columns swaps.
 **/
int32_t systematic_rev(matrix_ff2* A) {
    int32_t r, row, n;
    uint32_t found;
    vector *row_ptr = NULL, *v_ptr = NULL;
    vector mask = _mm256_setzero_si256(), tmp = _mm256_setzero_si256();
    row = A->nrows - 1;
    
    for (uint32_t c = A->ncols - 1; (int32_t)c >= A->ncols-A->nrows; c--){
        row_ptr = (vector*) row_ptr_matrix_ff2(A, row);

        // search for pivot
        found = 0;
        for (r = row; r >= 0; r--) {
            v_ptr = (vector*) row_ptr_matrix_ff2(A, r);
            // swap rows
            found |= bit_value((packed_t*) v_ptr, c);
            mask = _mm256_set1_epi64x(-bit_value((packed_t*) v_ptr, c));
            for (n = (c >> VECTORLOG2); n >= 0; n--) {
                tmp = row_ptr[n];
                tmp ^= v_ptr[n];
                v_ptr[n] ^= tmp & mask;
                row_ptr[n] ^= tmp & mask;
            }
        }
#ifdef CTGRIND_KEYGEN
    // failure if found is not 1, we can leak this if the failure is properly handled
    ct_unpoison(&found, sizeof(found));
#endif
        if (!found){
            return NTS_KEM_NON_SYSTEMATIC_MATRIX;
        }
    
        // eliminate column
        for (r = 0; r < A->nrows; r++) {
            v_ptr = (vector*) row_ptr_matrix_ff2(A, r);
            mask = _mm256_set1_epi64x(-bit_value((packed_t*) v_ptr, c));
            mask &= CT_is_not_equal(row, r);
            mask = -mask;
            for (n = (c >> VECTORLOG2); n >= 0; n--) {
                v_ptr[n] ^= row_ptr[n] & mask;
            }
        }
        row--;
    }

    return A->nrows;
}
#undef RREF_PACKED_BITS
