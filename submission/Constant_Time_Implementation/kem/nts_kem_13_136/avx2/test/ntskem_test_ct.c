/**
 *  ntskem_test.c
 *  NTS-KEM test
 *
 *  Parameter: NTS-KEM(13, 136)
 *  Platform: AVX2
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "api.h"
#include "random.h"
#include "ctgrind.h"
#include "ntskem_test_helper.h"


int test_decapsulation(int testid){
    int status = 1; 
    uint8_t *pk, *sk;
    uint8_t *encap_key, *decap_key, *ciphertext;

    pk = (uint8_t *)calloc(CRYPTO_PUBLICKEYBYTES, sizeof(uint8_t));
    sk = (uint8_t *)calloc(CRYPTO_SECRETKEYBYTES, sizeof(uint8_t));
    crypto_kem_keypair(pk, sk);

    ciphertext = (uint8_t *)calloc(CRYPTO_CIPHERTEXTBYTES, sizeof(uint8_t));
    encap_key = (uint8_t *)calloc(CRYPTO_BYTES, sizeof(uint8_t));
    decap_key = (uint8_t *)calloc(CRYPTO_BYTES, sizeof(uint8_t));
    // 1. valid ciphertext
    if(!testid || testid == 1){
        printf("Test 1 - Valid ciphertext:");

        nts_kem_test_encapsulate(pk, CRYPTO_PUBLICKEYBYTES, NTSKEM_T, ciphertext, encap_key);
        
        status &= !crypto_kem_dec(decap_key, ciphertext, sk);

        printf("DONE\n");
    }
    // 2. invalid ciphertext with hemming weight of e < tau
    if(!testid || testid == 2){
        printf("Test 2 - hamming weight < tau:");
        nts_kem_test_encapsulate(pk, CRYPTO_PUBLICKEYBYTES, NTSKEM_T-1, ciphertext, encap_key);
        
        status &= crypto_kem_dec(decap_key, ciphertext, sk);

        printf("DONE\n");
    }
    // 3. invalid ciphertext with hemming weight of e > tau
    if(!testid || testid == 3){
        printf("Test 3 - hamming weight > tau:");
        nts_kem_test_encapsulate(pk, CRYPTO_PUBLICKEYBYTES, NTSKEM_T+1, ciphertext, encap_key);

        status &= crypto_kem_dec(decap_key, ciphertext, sk);

        printf("DONE\n");
    }
    // 4. random ciphertext
    if(!testid || testid == 4){
        printf("Test 4 - random ciphertext:");
        randombytes(ciphertext, CRYPTO_CIPHERTEXTBYTES);
        status &= crypto_kem_dec(decap_key, ciphertext, sk);

        printf("DONE\n");
    }
    free(decap_key);
    free(encap_key);
    free(ciphertext);
    free(sk);
    free(pk);
    ct_unpoison(&status, sizeof(status));
    return status;
}

int test_encapsulation(){
    int status = 1; 
    uint8_t *pk, *sk;
    uint8_t *encap_key, *decap_key, *ciphertext;

    printf("Test Encapsulation:");
    pk = (uint8_t *)calloc(CRYPTO_PUBLICKEYBYTES, sizeof(uint8_t));
    sk = (uint8_t *)calloc(CRYPTO_SECRETKEYBYTES, sizeof(uint8_t));
    crypto_kem_keypair(pk, sk);

    ciphertext = (uint8_t *)calloc(CRYPTO_CIPHERTEXTBYTES, sizeof(uint8_t));
    encap_key = (uint8_t *)calloc(CRYPTO_BYTES, sizeof(uint8_t));
    decap_key = (uint8_t *)calloc(CRYPTO_BYTES, sizeof(uint8_t));

    status &= !crypto_kem_enc(ciphertext, encap_key, pk);


    free(decap_key);
    free(encap_key);
    free(ciphertext);
    free(sk);
    free(pk);

    printf("DONE\n");
    ct_unpoison(&status, sizeof(status));
    return status;
}

int test_key_generation(){
    int status = 1; 
    uint8_t *pk, *sk;

    printf("Test Key-Generation:");

    pk = (uint8_t *)calloc(CRYPTO_PUBLICKEYBYTES, sizeof(uint8_t));
    sk = (uint8_t *)calloc(CRYPTO_SECRETKEYBYTES, sizeof(uint8_t));
    status &= !crypto_kem_keypair(pk, sk);

    free(sk);
    free(pk);
    
    printf("DONE\n");
    ct_unpoison(&status, sizeof(status));
    return status;
}

int main(int argc, char* argv[])
{
    int i, it = 0, status = 1;
    FILE *fp = NULL;

    unsigned char entropy_input[] = {
        0xaa, 0xe7, 0xd7, 0x4e, 0x3c, 0x3a, 0x52, 0xdd,
        0x87, 0xc7, 0x2a, 0xa4, 0x38, 0x54, 0x7e, 0x37,
        0x1e, 0x97, 0x29, 0x78, 0x22, 0xa2, 0xcd, 0x83,
        0x43, 0x64, 0x84, 0xcf, 0x77, 0x6b, 0x9e, 0xa5,
        0x53, 0xf3, 0x50, 0xc5, 0xc7, 0x8d, 0x46, 0xb3,
        0xa5, 0xf2, 0xe3, 0x99, 0x63, 0x10, 0x1d, 0x10
    };
    unsigned char nonce[48];
    /*
     unsigned char *nonce = NULL;
     int32_t nonce_size = 48;
     */
    
    fprintf(stdout, "NTS-KEM(%d, %d) Test\n", NTSKEM_M, NTSKEM_T);

    if ((fp = fopen("/dev/urandom", "r"))) {
        if ((sizeof(entropy_input) != fread(entropy_input, 1, sizeof(entropy_input), fp)) ||
            (sizeof(nonce) != fread(nonce, 1, sizeof(nonce), fp))) {
            status = 0;
        }
    }
    fclose(fp);
    memcpy(&entropy_input[48-sizeof(it)], &it, sizeof(it));
    
    fprintf(stdout, "Seed: ");
    for (i=0; i<sizeof(entropy_input); i++) fprintf(stdout, "%02x", entropy_input[i]);
    fprintf(stdout, "\n"); fflush(stdout);
    
    randombytes_init(entropy_input, nonce, 256);

    #if defined(CTGRIND_DECAP)
        if (argc < 2){
            status &= test_decapsulation(0);
        } else {
            for(int i = 1; i < argc; i++) {
                status &= test_decapsulation((int)*argv[i] - '0');
            }
        }
    #endif
    #if defined(CTGRIND_ENCAP)
            status &= test_encapsulation();
    #endif
    #if defined(CTGRIND_KEYGEN)
        status &= test_key_generation();
    #endif
    return status;
}
