/**
 *  mergesort.c
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(13, 136)
 *  Platform: Intel 64-bit
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#include <stdint.h>
#include <string.h>
#include "mergesort.h"
#include "bits.h"
#include "nts_kem_errors.h"
#include "nts_kem_params.h"
#include "random.h"
#ifdef CTGRIND_KEYGEN
    #include "ctgrind.h"
#endif
#define NETSIZE (1 << NTS_KEM_PARAM_M)
#define POW2(n) (1U<<(n))

static inline void merge_key(uint16_t* val, uint16_t* key, uint32_t n, uint32_t offset);
static inline void compare_key(uint16_t* val, uint16_t* key, uint32_t i, uint32_t j, uint32_t offset);
static inline void compare_key32(uint16_t* val, uint32_t* key, uint32_t i, uint32_t j, uint32_t offset);
static inline void merge_key32(uint16_t* val, uint32_t* key, uint32_t n, uint32_t offset);
static inline void merge_key64(uint16_t* val, uint64_t* key, uint32_t n, uint32_t offset);
static inline void compare_key64(uint16_t* val, uint64_t* key, uint32_t i, uint32_t j, uint32_t offset);
void mergesort_key(uint16_t* val, uint16_t* key, uint32_t n);
void mergesort_key32(uint16_t* val, uint32_t* key, uint32_t n);
void mergesort_key64(uint16_t* val, uint64_t* key, uint32_t n) ;

/**
 *  Permute a vector in constant-time.
 *
 *  @param[in,out]  val     Vector to be permuted
 *  @param[in]      p       Permutation to be applied
 **/
void permute(uint16_t* val, const uint16_t* p) {
    uint16_t key[NETSIZE];
    memcpy(key, p, NETSIZE*sizeof(uint16_t));
    mergesort_key(val, key, NTS_KEM_PARAM_M);
}

/**
 *  Compute the inversion of a permutation in constant-time.
 *
 *  @param[in]      p       Permutation to be applied
 *  @param[out]     p_inv   Inverse permutation
 **/
void invert_permutation(const uint16_t* p, uint16_t* p_inv) {
    uint16_t key[NETSIZE];
    memcpy(key, p, NETSIZE*sizeof(uint16_t));
    for (int i = 0; i < NETSIZE; i ++) {
        p_inv[i] = i;
    }
    mergesort_key(p_inv, key, NTS_KEM_PARAM_M);
}

/**
 *  Create a uniformly random permutation in constant-time.
 *
 *  @param[out]      p       uniformly random permutation
 **/
void create_permutation(uint16_t* p) {
    uint64_t ran[NTS_KEM_PARAM_N];
    uint64_t t;
    do{
        t = 1;
        randombytes((uint8_t*) ran, sizeof(ran));
#ifdef CTGRIND_KEYGEN
        ct_poison(ran, sizeof(ran));
#endif
        for(int i = 0; i < NTS_KEM_PARAM_N; i++) {
            p[i] = i;
        }
        mergesort_key64(p, ran, NTS_KEM_PARAM_M);
        for(int i = 1; i < NTS_KEM_PARAM_N; i++) {
            t &= CT_is_not_equal64(ran[i], ran[i-1]);
        }
#ifdef CTGRIND_KEYGEN
    /*unpoison t, since it does not leak randomness, because we retry if t==0*/
    ct_unpoison(&t, sizeof(uint64_t));
#endif
    } while (CT_not64(t));
}

/** -------------------- Private methods -------------------- **/

void mergesort_key(uint16_t* val, uint16_t* key, uint32_t n) {
    for (uint32_t i = 1; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_key(val, key, i, j);
        }
    }
}

void mergesort_key32(uint16_t* val, uint32_t* key, uint32_t n) {
    for (uint32_t i = 1; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_key32(val, key, i, j);
        }
    }
}

void mergesort_key64(uint16_t* val, uint64_t* key, uint32_t n) {
    for (uint32_t i = 1; i <= n; i++) {
        for (uint32_t j = 0; j < POW2(n); j+=POW2(i)) {
            merge_key64(val, key, i, j);
        }
    }
}

static inline void compare_key(uint16_t* val, uint16_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    uint32_t c = CT_is_greater_than(key[offset+i], key[offset+j]);
    uint16_t t_key = key[offset+i] ^ key[offset+j];
    uint32_t t_val = val[offset+i] ^ val[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    val[offset+i] ^= (-c) & t_val;
    val[offset+j] ^= (-c) & t_val;
}

static inline void merge_key(uint16_t* val, uint16_t* key, uint32_t n, uint32_t offset) {
    for (uint32_t i = 0; i < POW2(n-1U); i++){
        compare_key(val, key, i, i+POW2(n-1U), offset);
    }
    for (uint32_t k = 1; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}
static inline void merge_key32(uint16_t* val, uint32_t* key, uint32_t n, uint32_t offset) {
    for (uint32_t i = 0; i < POW2(n-1U); i++){
        compare_key32(val, key, i, i+POW2(n-1U), offset);
    }
    for (uint32_t k = 1; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key32(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}

static inline void compare_key32(uint16_t* val, uint32_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    uint32_t c = CT_is_greater_than(key[offset+i], key[offset+j]);
    uint32_t t_key = key[offset+i] ^ key[offset+j];
    uint16_t t_val = val[offset+i] ^ val[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    val[offset+i] ^= (-c) & t_val;
    val[offset+j] ^= (-c) & t_val;
}

static inline void compare_key64(uint16_t* val, uint64_t* key, uint32_t i, uint32_t j, uint32_t offset) {
    uint64_t c = CT_is_greater_than64(key[offset+i], key[offset+j]);
    uint64_t t_key = key[offset+i] ^ key[offset+j];
    uint16_t t_val = val[offset+i] ^ val[offset+j];
    key[offset+i] ^= (-c) & t_key;
    key[offset+j] ^= (-c) & t_key;
    val[offset+i] ^= (-c) & t_val;
    val[offset+j] ^= (-c) & t_val;
}

static inline void merge_key64(uint16_t* val, uint64_t* key, uint32_t n, uint32_t offset) {
    for (uint32_t i = 0; i < POW2(n-1U); i++){
        compare_key64(val, key, i, i+POW2(n-1U), offset);
    }
    for (uint32_t k = 1; k < n; k++) {
        for (uint32_t j = 0; j < (POW2(n)-POW2(n-k)); j+=POW2(n-k)) {
            for (uint32_t i = j + POW2(n-k-1U); i < j + POW2(n-k); i++) {
                compare_key64(val, key, i, i+POW2(n-k-1U), offset);
            }
        }
    }
}
