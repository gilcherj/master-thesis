CC = gcc
OPENSSLDIR = /usr/local/opt/openssl
LIBCRYPTO = $(OPENSSLDIR)/lib/libcrypto.a

MAKELIB = libtool -static -o $@ $^
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	OPENSSLDIR = /usr
    MAKELIB = ar cru $@ $^ && ranlib $@
	CFLAGS += -DLINUX -D_POSIX_C_SOURCE=200112L
	LIBCRYPTO = -lcrypto
	#LIBS += -lbsd
endif

INCLUDES = -I. -Ibit-slice -Inist -I$(OPENSSLDIR)/include
LIBS += $(LIBCRYPTO) -ldl 

CFLAGS += -DNIST_DRBG_AES 
#CFLAGS += -O3 -ansi -std=c99 -fomit-frame-pointer -fwrapv -Wpedantic -Wall -Werror $(INCLUDES)
CFLAGS += -ansi -std=c99 -fwrapv -Wpedantic -Wall -Werror $(INCLUDES)

LIBTARGET = lib
BINTARGET = bin

_ODIR = .obj
_ODIRKAT = .objkat
ODIR = lib bin $(_ODIR) $(_ODIRKAT) 

CTFLAGS = -ggdb3 -I/local/include
DCTGRIND = -DCTGRIND_ENCAP -DCTGRIND_DECAP -DCTGRIND_KEYGEN

default : CFLAGS += -DNO_PRAGMA
default : exec

exec: CFLAGS += -fomit-frame-pointer -O3
exec: all

use_pragma : exec

benchmark: CFLAGS += -DBENCHMARK
benchmark: exec

no_loop_vectorize_gcc: CFLAGS += -fno-tree-loop-vectorize
no_loop_vectorize_gcc: exec

no_loop_vectorize_clang: CFLAGS += -fno-vectorize
no_loop_vectorize_clang: exec

testO0 : CFLAGS += -O0
testO0 : test 

testO1 : CFLAGS += -O1
testO1 : test 

testO2 : CFLAGS += -O2
testO2 : test 

testO3 : CFLAGS += -O3
testO3 : test 


test : CFLAGS += $(CTFLAGS) $(DCTGRIND) $(DEFAULTFLAGS)
test: testall

test_encapO0 : CFLAGS += -O0
test_encapO0 : test_encap

test_encapO1 : CFLAGS += -O1
test_encapO1 : test_encap

test_encapO2 : CFLAGS += -O2
test_encapO2 : test_encap

test_encapO3 : CFLAGS += -O3
test_encapO3 : test_encap

test_encap: CFLAGS += $(CTFLAGS) -DCTGRIND_ENCAP $(DEFAULTFLAGS)
test_encap: testall

test_decapO0 : CFLAGS += -O0
test_decapO0: test_decap

test_decapO1 : CFLAGS += -O1
test_decapO1: test_decap

test_decapO2 : CFLAGS += -O2
test_decapO2: test_decap

test_decapO3 : CFLAGS += -O3
test_decapO3: test_decap

test_decap: CFLAGS += $(CTFLAGS) -DCTGRIND_DECAP $(DEFAULTFLAGS)
test_decap: testall

test_keygen: CFLAGS += $(CTFLAGS) -DCTGRIND_KEYGEN $(DEFAULTFLAGS)
test_keygen: testall

test_keygenO0: CFLAGS += -O0
test_keygenO0: test_keygen

test_keygenO1: CFLAGS += -O1
test_keygenO1: test_keygen

test_keygenO2: CFLAGS += -O2
test_keygenO2: test_keygen

test_keygenO3: CFLAGS += -O3
test_keygenO3: test_keygen

testall : $(BINTARGET)/ntskem-13-80-ct-ctgrind

all : $(BINTARGET)/ntskem-13-80-ct-test $(BINTARGET)/ntskem-13-80-ct-kat

_DEPS = 
DEPS = $(patsubst %,$(INCLUDEDIR)/%,$(_DEPS))

_OBJS = bit-slice/bitslice_bma_64.o bit-slice/bitslice_fft_64.o bit-slice/vector_utils.o \
		ff.o keccak.o kem.o m4r.o matrix_ff2.o nts_kem.o polynomial.o random.o \
		nist/aes_drbg.o mergesort.o utils.o gauss_elimination.o mem.o bit_mergesort.o
OBJS = $(patsubst %,$(_ODIR)/%,$(_OBJS))
OBJSKAT = $(patsubst %,$(_ODIRKAT)/%,$(_OBJS))

$(ODIR):
	mkdir -p lib
	mkdir -p bin
	mkdir -p .obj/bit-slice
	mkdir -p .obj/nist
	mkdir -p .objkat/bit-slice
	mkdir -p .objkat/nist

$(_ODIR)/%.o: %.c $(_ODIR) $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $< 

$(_ODIRKAT)/%.o: %.c $(_ODIRKAT) $(DEPS)
	$(CC) $(CFLAGS) -DINTERMEDIATE_VALUES=2 -c -o $@ $< 

$(LIBTARGET)/libntskem-13-80-ct.a: $(OBJS)
	$(MAKELIB)

$(BINTARGET)/ntskem-13-80-ct-test: test/main.c test/ntskem_test.c $(LIBTARGET)/libntskem-13-80-ct.a 
	$(CC) $(CFLAGS) -DNTSKEM_M=13 -DNTSKEM_T=80 -o $@ $(LDFLAGS) $^ $(LIBS) 

$(BINTARGET)/ntskem-13-80-ct-kat: nist/PQCgenKAT_kem.c $(LIBTARGET)/libntskem-13-80-ct.a
	$(CC) $(CFLAGS) -o $@ $(LDFLAGS) $^ $(LIBS)

$(BINTARGET)/ntskem-13-80-ct-ctgrind: test/ntskem_test_ct.c test/ntskem_test_helper.c $(LIBTARGET)/libntskem-13-80-ct.a 
	$(CC) $(CFLAGS) -DNTSKEM_M=13 -DNTSKEM_T=80 -o $@ $(LDFLAGS) $^ $(LIBS) -L. -lctgrind

.PHONY: clean

clean:
	rm -rf $(ODIR)
