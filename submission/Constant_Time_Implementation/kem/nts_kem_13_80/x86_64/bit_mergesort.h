/**
 *  bit_mergesort.h
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(13, 80)
 *  Platform: Intel 64-bit
 *
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#ifndef __BIT_MERGESORT_H
#define __BIT_MERGESORT_H
#include <stdint.h>
#include "nts_kem_params.h"

#define CONTROLSIZE NTS_KEM_PARAM_M*NTS_KEM_PARAM_M*(NTS_KEM_PARAM_N >> 6)
#define CONTROLBYTES (CONTROLSIZE) << 3


/**
 *  Computes swaps required to apply a permutation.
 *  
 *  @param[in,out]  in          Permutation, will be sorted on return
 *  @param[in]      n           Degree of the Network
 *  @param[out]     control     Swaps required to apply the permutation 
 **/
void computeControlBits(uint16_t* in, uint32_t n, uint64_t* control);

/**
 *  Apply inverse of the permutation defined via control mask to a bit vector.
 *  
 *  @param[in,out]  arr         bit vector to be permuted
 *  @param[out]     control     Swaps defining the permutation 
 **/
void bitPermuteInv(uint64_t* arr, const uint64_t* control);

#endif
