#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

./ctgrind_decap.sh
./ctgrind_encap.sh
./ctgrind_keygen.sh
./ctgrind_keygen_rref.sh
./ctgrind_full.sh