/**
 *  timing.h
 *  NTS-KEM
 *
 *  Parameter: NTS-KEM(13, 80)
 *  Platform: AVX2
 * 
 *  This file is part of the constant-time implementation of NTS-KEM
 *  submitted as part of NIST Post-Quantum Cryptography
 *  Standardization Process.
 **/

#ifndef __TIMING_H
#define __TIMING_H

#include <stdint.h>
#include <inttypes.h>
#include <time.h>
#include <sys/resource.h>
#include <stdio.h>
#include "utils.h"

#define BILLION 1000000000ULL

static double dn[5] = {0., 0.25, 0.5, 0.75, 1.};

typedef struct timing_data {
    double q[5];
    double n_prime[5];
    uint64_t n[5];
    uint64_t timings;
    uint64_t start;
    uint64_t end;
    uint64_t counts;
} timing_data_t;


timing_data_t keygen;
timing_data_t keygenSetup;
timing_data_t keygenGCD;
timing_data_t keygenFormalDerivative;
timing_data_t keygenGoppaPolyCheck;
timing_data_t keygenCreateGoppa;
timing_data_t keygenSampleRandomness;
timing_data_t keygenRandomUint16;
timing_data_t keygenPermuteA;
timing_data_t keygenPermuteH;
timing_data_t keygenCreateParityMat;
timing_data_t keygenFisherYates;
timing_data_t keygenInvPermutation;
timing_data_t keygenSystematicG;
timing_data_t keygenRowEchelon;
timing_data_t keygenPermuteHmat;
timing_data_t keygenFindPermutationHmat;
timing_data_t keygenTransposeHmat;
timing_data_t keygenCreateQ;
timing_data_t keygenMatrixClone;
timing_data_t keygenCreateZ;
timing_data_t keygenSerialise;
timing_data_t keygenFree;

timing_data_t encap;
timing_data_t encapSetup;
timing_data_t encapRandomVector;
timing_data_t encapRandomUint16;
timing_data_t encapFun;
timing_data_t encapCleanup;

timing_data_t encapsCore;
timing_data_t encapsCoreSetup;
timing_data_t encapsHashE;
timing_data_t encapsMatrixMult;
timing_data_t encapsCipherError;
timing_data_t encapsHashK_r;

timing_data_t decapsulate;
timing_data_t precomputeControlBits;
timing_data_t decap;
timing_data_t decapSetup;
timing_data_t decapSyndrome;
timing_data_t decapBMA;
timing_data_t decapFFT;
timing_data_t decapE_prime;
timing_data_t decapsEncap;
timing_data_t decapsErrorCorrect;
timing_data_t decapsInvPermutation;
timing_data_t decapHashE;
timing_data_t decapCheckCodeword;
timing_data_t decapHashK_r;
timing_data_t decapCleanup;

timing_data_t RandomUInt16;

static inline uint64_t gettime() {
    return cpucycles();
}


static inline void tic(timing_data_t* td) {
    td->start = gettime();
}

static inline void swap(double a[5], int i, int j) {
    uint64_t t; 
    if (a[i] > a[j]) {
        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

}
static inline void sort5(double a[5]) {
    swap(a, 0, 1);
    swap(a, 3, 4);
    swap(a, 2, 4);
    swap(a, 2, 3);
    swap(a, 1, 4);
    swap(a, 0, 3);
    swap(a, 0, 2);
    swap(a, 1, 3);
    swap(a, 1, 2);
}

static inline void toc(timing_data_t* td) {
    td->end = gettime();
    uint64_t t;
    int64_t sign;
    double d, q_prime;
    int k = 0;
    t = td->end - td->start;
    if (td->counts < 5) {
        td->q[td->counts] = t;
    }
    else {
        /* find k s.t. q_k <= t < q_{k+1} and adjust extremes */
        if (t < td->q[0]) {
            td->q[0] = t;
            k = 1;
        }
        else if( t < td->q[1]) {
            k = 1;
        }
        else if (t < td->q[2]) {
            k = 2;
        }
        else if (t < td->q[3]) {
            k = 3;
        }
        else if (t < td->q[4]) {
            k = 4;
        }
        else {
            td->q[4] = t;
            k = 4;
        }
        /* update markers */
        for (int i = k; i < 5; i++) {
            td->n[i]++;
        }
        for (int i = 0; i < 5; i++) {
            td->n_prime[i] += dn[i];
        }
        /* adjust marker heights */
        for (int i = 1; i < 4; i++) {
            d = td->n_prime[i] - td->n[i];
            if (((d >= 1) && (td->n[i+1] - td->n[i] > 1)) || ((d <= -1) && (td->n[i+1] - td->n[i] < -1))) {
                sign = d < 0 ? -1 : 1;
                q_prime = (td->n[i+1] - td->n[i] - d) * (td->q[i] - td->q[i-1]) / (td->n[i] - td->n[i-1]);
                q_prime += (td->n[i] - td->n[i-1] + d) * (td->q[i+1] - td->q[i]) / (td->n[i+1] - td->n[i]);
                q_prime *= d;
                q_prime /= (td->n[i+1] - td->n[i-1]);
                q_prime += td->q[i];
                if (td->q[i-1] < q_prime && q_prime < td->q[i+1]) {
                    td->q[i] = q_prime;
                }
                else {
                    td -> q[i] += sign * (td->q[i+sign] - td->q[i])/(td->n[i+sign] - td->n[i]);
                }
                td->n[i] += sign;
            }
        }
    }
    td->counts++;
    if (td->counts == 5){   
        sort5(td->q);
    }
    td->timings += t;
}

static inline void setup_timing(timing_data_t* td) {
    for(int i = 0; i < 5; i++) {
        td->q[i] = 0;
        td->n[i] = i+1;
        td->n_prime[i] = i+1;
    }
    td->timings = 0;
    td->counts = 0;
}

static inline void print_number(uint64_t n) {
#define THOUSAND    1000UL
#define MILLION     1000000UL
#define BILLIONL    1000000000UL
#define TRILLION    1000000000000UL
#define QUADRILLION 1000000000000000UL
#define QUINTILLION 1000000000000000000UL
    int start = 1;
    int d = 3;
    if (n / QUINTILLION) {
        printf("%*"PRIu64"'", d, n / QUINTILLION);
        start = 0;
        n %= QUINTILLION;
    } else {
        d += 4;
    }
    if (!start || (n / QUADRILLION)) {
        printf(start ? "%*"PRIu64"'" : "%0*"PRIu64"'", d, n / QUADRILLION);
        start = 0;
        n %= QUADRILLION;
        d = 3;
    } else {
        d += 4;
    }
    if (!start || (n / TRILLION)) {
        printf(start ? "%*"PRIu64"'" : "%0*"PRIu64"'", d, n / TRILLION);
        start = 0;
        n %= TRILLION;
        d = 3;
    } else {
        d += 4;
    }
    if (!start || (n / BILLIONL)) {
        printf(start ? "%*"PRIu64"'" : "%0*"PRIu64"'", d, n / BILLIONL);
        start = 0;
        n %= BILLIONL;
        d = 3;
    } else {
        d += 4;
    }
    if (!start || (n / MILLION)) {
        printf(start ? "%*"PRIu64"'" : "%0*"PRIu64"'", d, n / MILLION);
        start = 0;
        n %= MILLION;
        d = 3;
    } else {
        d += 4;
    }
    if (!start || (n / THOUSAND)) {
        printf(start ? "%*"PRIu64"'" : "%0*"PRIu64"'", d, n / THOUSAND);
        start = 0;
        n %= THOUSAND;
        d = 3;
    } else {
        d += 4;
    }
    printf(start ? "%*"PRIu64"" : "%0*"PRIu64"", d, n);
    start = 0;
}

static inline void print_timing(timing_data_t* td, char* name, char* prefix, uint32_t numtests) {
    //printf("Average Time spent in %s : %LF us\n", name, (long double)td->timings/ (td->counts * 1000));
    //printf("Average Time spent in %s : %LF clocks\n", name, (long double)td->timings/ (td->counts));
    printf("%sAverage Time spent in %s:\n", prefix, name);
    printf("%s-cycles:       ", prefix);
    print_number(td->timings / (numtests ? numtests : 1));
    printf(",\tavg: ");
    print_number(td->timings / (td->counts ? td->counts : 1));
    printf("\n");
    
    printf("%s-called:       ", prefix);
    print_number(td->counts);
    printf(",\tavg: ");
    printf("%27.2f", (double)td->counts / (numtests ? numtests : 1));
    printf("\n");
    printf("%s-min:          ", prefix);
    print_number(td->q[0]);
    printf("\n");
    printf("%s-max:          ", prefix);
    print_number(td->q[4]);
    printf("\n");
    printf("%s-1st-quartile: ", prefix);
    print_number(td->q[1]);
    printf("\n");
    printf("%s-median:       ", prefix);
    print_number(td->q[2]);
    printf("\n");
    printf("%s-3rd-quartile: ", prefix);
    print_number(td->q[3]);
    printf("\n");

}

static inline void setup_all_timing() {
    setup_timing(&keygen);
    setup_timing(&keygenSetup);
    setup_timing(&keygenGCD);
    setup_timing(&keygenFormalDerivative);
    setup_timing(&keygenGoppaPolyCheck);
    setup_timing(&keygenCreateGoppa);
    setup_timing(&keygenSampleRandomness);
    setup_timing(&keygenPermuteA);
    setup_timing(&keygenPermuteH);
    setup_timing(&keygenRandomUint16);
    setup_timing(&keygenFisherYates);
    setup_timing(&keygenInvPermutation);
    setup_timing(&keygenCreateParityMat);
    setup_timing(&keygenSystematicG);
    setup_timing(&keygenRowEchelon);
    setup_timing(&keygenPermuteHmat);
    setup_timing(&keygenFindPermutationHmat);
    setup_timing(&keygenTransposeHmat);
    setup_timing(&keygenCreateQ);
    setup_timing(&keygenMatrixClone);
    setup_timing(&keygenCreateZ);
    setup_timing(&keygenSerialise);
    setup_timing(&keygenFree);

    setup_timing(&encap);
    setup_timing(&encapSetup);
    setup_timing(&encapRandomVector);
    setup_timing(&encapRandomUint16);
    setup_timing(&encapFun);
    setup_timing(&encapCleanup);

    setup_timing(&encapsCore);
    setup_timing(&encapsCoreSetup);
    setup_timing(&encapsHashE);
    setup_timing(&encapsMatrixMult);
    setup_timing(&encapsCipherError);
    setup_timing(&encapsHashK_r);

    setup_timing(&decapsulate);
    setup_timing(&precomputeControlBits);
    setup_timing(&decap);
    setup_timing(&decapSetup);
    setup_timing(&decapSyndrome);
    setup_timing(&decapBMA);
    setup_timing(&decapFFT);
    setup_timing(&decapE_prime);
    setup_timing(&decapsEncap);
    setup_timing(&decapsErrorCorrect);
    setup_timing(&decapsInvPermutation);
    setup_timing(&decapHashE);
    setup_timing(&decapCheckCodeword);
    setup_timing(&decapHashK_r);
    setup_timing(&decapCleanup);

    setup_timing(&RandomUInt16);
}

static inline void print_all_timing(uint32_t numtests) {
    printf("All timings in num cycles\n");
    printf("Key-Generation:\n");
    print_timing(&keygen, "key-generation", "|-",  numtests);
    print_timing(&keygenSetup, "setup", "| |-", numtests);
    print_timing(&keygenCreateGoppa, "create_random_goppa_polynomial", "| |-", numtests);
    print_timing(&keygenSampleRandomness, "randombytes", "| | |-", numtests);
    print_timing(&keygenGoppaPolyCheck, "is_valid_goppa_polynomial", "| | |-", numtests);
    print_timing(&keygenFormalDerivative, "formal_derivative_polynomial", "| | | |-", numtests);
    print_timing(&keygenGCD, "computing deg(gcd)", "| | | |-", numtests);
    print_timing(&keygenFisherYates, "generating permutation", "| |-", numtests);
    print_timing(&keygenRandomUint16, "random_uint16_bounded", "| | |-",numtests);
    print_timing(&keygenCreateQ, "create_matrix_G", "| |-", numtests);
    print_timing(&keygenCreateParityMat, "creating parity check matrix H", "| | |-", numtests);
    print_timing(&keygenInvPermutation, "invert_permutation", "| | |-", numtests);
    print_timing(&keygenPermuteA, "permuting a_prime", "| | |-", numtests);
    print_timing(&keygenPermuteH, "permuting h", "| | |-", numtests);
    print_timing(&keygenSystematicG, "creating systematic G", "| | |-", numtests);
    print_timing(&keygenRowEchelon, "reduce_row_echelon_matrix_ff2", "| | | |-", numtests);
    print_timing(&keygenFindPermutationHmat, "finding the columns to permute", "| | | |-", numtests);
    print_timing(&keygenPermuteHmat, "permuting columns of H", "| | | |-", numtests);
    print_timing(&keygenTransposeHmat, "transposing H", "| | | |-", numtests);
    print_timing(&keygenMatrixClone, "cloning Q", "| | | |-", numtests);
    print_timing(&keygenCreateZ, "sampling z", "| |-", numtests);
    print_timing(&keygenSerialise, "serializing the keys", "| |-", numtests);
    print_timing(&keygenFree, "cleanup", "| |-", numtests);

    printf("Encapsulation:\n");
    print_timing(&encap, "encapsulation", "|-", numtests);
    print_timing(&encapSetup, "setup", "| |-", numtests);
    print_timing(&encapRandomVector, "random_vector", "| |-", numtests);
    print_timing(&encapRandomUint16, "random_uint16_bounded", "| | |-",numtests);
    print_timing(&encapFun, "core encapsulation function", "| |-", numtests);
    print_timing(&encapCleanup, "cleanup", "| |-", numtests);

    printf("Decapsulation:\n");
    print_timing(&decapsulate, "decapsulation", "|-", numtests);
    print_timing(&precomputeControlBits, "precomputing control bits", "| |-", numtests);
    print_timing(&decap, "decapsulation_function", "| |-", numtests);
    print_timing(&decapSetup, "setup decapsulation", "| | |-", numtests);
    print_timing(&decapSyndrome, "syndrome computation", "| | |-", numtests);
    print_timing(&decapBMA, "BMA", "| | |-", numtests);
    print_timing(&decapFFT, "FFT", "| | |-", numtests);
    print_timing(&decapE_prime, "computing e'", "| | |-", numtests);
    print_timing(&decapsErrorCorrect, "permute_error", "| | |-", numtests);
    print_timing(&decapsInvPermutation, "inverting Permuation", "| | | |-", numtests);
    print_timing(&decapsEncap,  "re-encapsulation", "| | |-", numtests);
    print_timing(&decapCheckCodeword, "checking c' = c* and wt(e) = τ", "| | |-", numtests);
    print_timing(&decapHashK_r, "computing k_r", "| | |-", numtests);
    print_timing(&decapCleanup, "cleanup", "| | |-", numtests);
    
    printf("Other:\n");
    print_timing(&RandomUInt16, "random_uint_bounded", "|-", numtests);
    print_timing(&encapsCore, "encapsulate()", "|-", numtests);
    print_timing(&encapsCoreSetup, "setup in encapsulate", "| |-",numtests);
    print_timing(&encapsHashE, "computing H(e)", "| |-", numtests);
    print_timing(&encapsMatrixMult, "matrix multiplication", "| |-", numtests);
    print_timing(&encapsCipherError, "perturbing ciphertext with e", "| |-", numtests);
    print_timing(&encapsHashK_r, "computing k_r", "| |-", numtests);
}

static inline void printCSVLine(FILE *fp, timing_data_t* td, const char* name, uint32_t numtests) {
    /* name,runs,avg_per_run,avg_per_call,called_tot,called_avg,min,max,1st_quartile,median,3rd_quartile */
    fprintf(fp,"\"%s\",%u,%lu,%lu,%lu,%.2f,%.0f,%.0f,%.0f,%.0f,%.0f\n",
            name, numtests, td->timings / (numtests ? numtests : 1), td->timings / (td->counts ? td->counts : 1),
            td->counts, (double)td->counts / (numtests ? numtests : 1), td->q[0], td->q[4],
            td->q[1], td->q[2], td->q[3]);
}

static inline void printToCSV(const char* name, uint32_t numtests) {
    FILE *fp = NULL;
    if (!(fp = fopen(name, "w"))) {
        return;
    }
    fprintf(fp,"name,runs,avg_per_run,avg_per_call,called_tot,called_avg,min,max,1st_quartile,median,3rd_quartile\n");

    printCSVLine(fp, &keygen, "key-generation",  numtests);
    printCSVLine(fp, &keygenSetup, "setup keygen", numtests);
    printCSVLine(fp, &keygenCreateGoppa, "create_random_goppa_polynomial", numtests);
    printCSVLine(fp, &keygenSampleRandomness, "randombytes", numtests);
    printCSVLine(fp, &keygenGoppaPolyCheck, "is_valid_goppa_polynomial", numtests);
    printCSVLine(fp, &keygenFormalDerivative, "formal_derivative_polynomial", numtests);
    printCSVLine(fp, &keygenGCD, "computing deg(gcd)", numtests);
    printCSVLine(fp, &keygenFisherYates, "generating permutation", numtests);
    printCSVLine(fp, &keygenRandomUint16, "random_uint16_bounded keygen", numtests);
    printCSVLine(fp, &keygenCreateQ, "create_matrix_G", numtests);
    printCSVLine(fp, &keygenCreateParityMat, "creating parity check matrix H", numtests);
    printCSVLine(fp, &keygenInvPermutation, "invert_permutation", numtests);
    printCSVLine(fp, &keygenPermuteA, "permuting a_prime", numtests);
    printCSVLine(fp, &keygenPermuteH, "permuting h", numtests);
    printCSVLine(fp, &keygenSystematicG, "creating systematic G", numtests);
    printCSVLine(fp, &keygenRowEchelon, "reduce_row_echelon_matrix_ff2", numtests);
    printCSVLine(fp, &keygenFindPermutationHmat, "finding the columns to permute",  numtests);
    printCSVLine(fp, &keygenPermuteHmat, "permuting columns of H", numtests);
    printCSVLine(fp, &keygenTransposeHmat, "transposing H",  numtests);
    printCSVLine(fp, &keygenMatrixClone, "cloning Q", numtests);
    printCSVLine(fp, &keygenCreateZ, "sampling z", numtests);
    printCSVLine(fp, &keygenSerialise, "serializing the keys", numtests);
    printCSVLine(fp, &keygenFree, "cleanup keygen", numtests);

    printCSVLine(fp, &encap, "encapsulation", numtests);
    printCSVLine(fp, &encapSetup, "setup encapsulation", numtests);
    printCSVLine(fp, &encapRandomVector, "random_vector", numtests);
    printCSVLine(fp, &encapRandomUint16, "random_uint16_bounded encapsulation", numtests);
    printCSVLine(fp, &encapFun, "core encapsulation function", numtests);
    printCSVLine(fp, &encapCleanup, "cleanup encapsulation", numtests);

    printCSVLine(fp, &decapsulate, "decapsulation", numtests);
    printCSVLine(fp, &precomputeControlBits, "precomputing control bits", numtests);
    printCSVLine(fp, &decap, "decapsulation_function", numtests);
    printCSVLine(fp, &decapSetup, "setup decapsulation", numtests);
    printCSVLine(fp, &decapSyndrome, "syndrome computation", numtests);
    printCSVLine(fp, &decapBMA, "BMA", numtests);
    printCSVLine(fp, &decapFFT, "FFT", numtests);
    printCSVLine(fp, &decapE_prime, "computing e'", numtests);
    printCSVLine(fp, &decapsErrorCorrect, "permute_error", numtests);
    printCSVLine(fp, &decapsInvPermutation, "inverting Permuation", numtests);
    printCSVLine(fp, &decapsEncap,  "re-encapsulation", numtests);
    printCSVLine(fp, &decapCheckCodeword, "checking c' = c* and wt(e) = τ", numtests);
    printCSVLine(fp, &decapHashK_r, "computing k_r", numtests);
    printCSVLine(fp, &decapCleanup, "cleanup decapsulation", numtests);
    
    printCSVLine(fp, &RandomUInt16, "random_uint16_bounded", numtests);
    printCSVLine(fp, &encapsCore, "encapsulate()", numtests);
    printCSVLine(fp, &encapsCoreSetup, "setup in encapsulate()", numtests);
    printCSVLine(fp, &encapsHashE, "computing H(e)", numtests);
    printCSVLine(fp, &encapsMatrixMult, "matrix multiplication", numtests);
    printCSVLine(fp, &encapsCipherError, "perturbing ciphertext with e", numtests);
    printCSVLine(fp, &encapsHashK_r, "computing k_r in encapsulate()", numtests);

    fclose(fp);
}
#endif
